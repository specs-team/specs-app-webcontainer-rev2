var jq = $.noConflict();
/*App Module */

angular.module('underscore', [])
	.factory('_', function () {
		return window._; // assumes underscore has already been loaded on the page
	});


//Define an angular module for our app
angular.module('SlaApp', [//'SlaApp.ranking', 
                          'SlaApp.negotiate', 
                          'SlaApp.sign', 
                          'SlaApp.implement', 
                           'SlaApp.monitor', 
                          'SlaApp.controllers', 'SlaApp.directives', 'SlaApp.config', 'SlaApp.filters', 'SlaApp.services',
                           'ngAnimate', 'ui.router', 'angular-loading-bar', 'underscore'])

// configuring our routes 
.config(function ($stateProvider, $urlRouterProvider) {
  
    $stateProvider
        .state('welcome', {
            url: '/welcome',
            //templateUrl: 'views/welcome.html',
            templateUrl: 'resources/views/welcome.html',
            controller: 'SlaCtrl'
        })
        .state('ranking', {
            url: '/',
            template: '<div>ranking</div>',
            //controller: 'IndexCtrl'
        })
        .state('negotiate', {
            url: '/negotiate',
            abstract: true,
            //templateUrl: 'views/main-negotiate.html',
            templateUrl: 'resources/views/main-negotiate.html',
            controller: 'NegotiateCtrl'
        })
        .state('sign', {
            url: '/sign',
            abstract: true,
            //templateUrl: 'views/main-sign.html',
            templateUrl: 'resources/views/main-sign.html',
            controller: 'SignCtrl'
        })
        .state('implement', {
            url: '/implement',
            abstract: true,
            //templateUrl: 'views/main-implement.html',
            templateUrl: 'resources/views/main-implement.html',
            controller: 'ImplementCtrl'
        })
        .state('monitor', {
            url: '/monitor',
            abstract: true,
            //templateUrl: 'views/main-monitor.html',
            templateUrl: 'resources/views/main-monitor.html',
            controller: 'MonitorCtrl'
        });
        
    	//PATH CALLED AT FIRST TIME BY ANGULAR CONTROLLER
        $urlRouterProvider.otherwise('/welcome');
})

.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
}])






;

/*angular.module('SlaApp.ranking', ['ui.router'])
.config(function ($stateProvider) {
  
    $stateProvider
        .state('ranking.home', {
            url: '/start',
            template: '<div>ranking</div>',
            //controller: 'LoginCtrl'
        });
});*/

angular.module('SlaApp.negotiate', ['SlaApp.negotiate.controllers', 'SlaApp.negotiate.factories', 'ngAnimate', 'ui.router'])
.config(function ($stateProvider) {
	
    $stateProvider
        .state('negotiate.start', {
            url: '/start',
            //templateUrl: 'views/negotiate/form-start.html',
            templateUrl: 'resources/views/negotiate/form-start.html',
            controller: 'negotiate.StartCtrl'
        })
        .state('negotiate.service', {
            url: '/service',
            //templateUrl: 'views/negotiate/form-service.html',
            templateUrl: 'resources/views/negotiate/form-service.html',
            controller: 'negotiate.ServiceCtrl'
        })
        .state('negotiate.capability', {
            url: '/capability',
            //templateUrl: 'views/negotiate/form-capability.html',
            templateUrl: 'resources/views/negotiate/form-capability.html',
            controller: 'negotiate.CapabilityCtrl'
        })
        .state('negotiate.security', {
            url: '/security',
            //templateUrl: 'views/negotiate/form-security.html',
            templateUrl: 'resources/views/negotiate/form-security.html',
            controller: 'negotiate.SecurityCtrl'
        })
        .state('negotiate.agreement', {
            url: '/agreement',
            //templateUrl: 'views/negotiate/form-agreement.html',
            templateUrl: 'resources/views/negotiate/form-agreement.html',
            controller: 'negotiate.AgreementCtrl'
        })
        .state('negotiate.overview', {
            url: '/overview',
            //templateUrl: 'views/negotiate/form-overview.html',
            templateUrl: 'resources/views/negotiate/form-overview.html',
            controller: 'negotiate.OverviewCtrl'
        })
        
});


angular.module('SlaApp.sign', ['SlaApp.sign.controllers', 'SlaApp.sign.factories', 'ngAnimate','ui.router'])
.config(function ($stateProvider) {
  
    $stateProvider
        .state('sign.start', {
            url: '/start',
            //templateUrl: 'views/sign/form-start.html',
            templateUrl: 'resources/views/sign/form-start.html',
            controller: 'sign.StartCtrl'
        })
});


angular.module('SlaApp.implement', ['SlaApp.implement.controllers', 'SlaApp.implement.factories', 'ngAnimate','ui.router'])
.config(function ($stateProvider) {
  
    $stateProvider
        .state('implement.start', {
            url: '/start',
            //templateUrl: 'views/implement/form-start.html',
            templateUrl: 'resources/views/implement/form-start.html',
            controller: 'implement.StartCtrl'
        });
});


//angular.module('SlaApp.monitor', ['SlaApp.monitor.controllers', 'SlaApp.monitor.factories', 'ngAnimate','ui.router', '720kb.tooltips'])
angular.module('SlaApp.monitor', ['SlaApp.monitor.controllers', 'SlaApp.monitor.factories', 'ngAnimate','ui.router'])
.config(function ($stateProvider) {
  
    $stateProvider
        .state('monitor.start', {
            url: '/start',
            //templateUrl: 'views/monitor/form-start.html',
            templateUrl: 'resources/views/monitor/form-start.html',            
            controller: 'monitor.StartCtrl'
        })
        .state('monitor.summary', {
            url: '/summary',
            //templateUrl: 'views/monitor/form-summary.html',
            templateUrl: 'resources/views/monitor/form-summary.html',
            controller: 'monitor.SummaryCtrl'
        })
})

;














