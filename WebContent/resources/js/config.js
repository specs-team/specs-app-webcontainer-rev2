
angular.module('SlaApp.config', [])
	.constant('NEGOTIATION_API', {
        Services: 'rest/services/',
        Capabilities: 'rest/capabilities/',  
        Securities: 'rest/securities/',
        Agreements: 'rest/agreements/',
        Overview: 'rest/overview/',
        Evaluate: 'rest/evaluate/',
        Rievaluate: 'rest/rievaluate/',
        Offer: 'rest/submitOffer/',
        OfferXML: 'rest/offerxml/',
        OfferSynthetic: 'rest/offersynthetic/',
        ServiceXML: 'rest/servicexml/',
    	Detail: 'rest/detailendpoint'
	})

    .constant('NEGOTIATION_METRIC_DETAIL', {
        Url: "http://apps.specs-project.eu:8080/" + 'metric-catalogue-app/show_abstract_metric.jsp?metric_id='
    })

    .constant('SIGN_API', {
        Signs: 'rest/incomplete/pending/',
        Sign: 'rest/sign/',
        SignXML:'rest/signxml/',
        TemplateXML:'rest/slaxml/',
        Synt: 'rest/syntsla/'
    })

    .constant('IMPLEMENT_API', {
        Implements: 'rest/incomplete/signed/',
        WaitToSign: 'rest/incomplete/negotiating/',
        Implement: 'rest/implement/'
    })

    .constant('MONITOR_API', {
        Monitors: 'rest/monitoring/',
        MonitorXML: 'rest/slaxml/',
        MetricsDetails: 'rest/metricsDetails/',
        Implemented: 'rest/implemented/',
        Nodes: 'rest/nodes/',
        Synth: 'rest/synt/',
        Terminate: 'rest/terminate/',
        Annotation: 'rest/annotation/',
        SyntSla: 'rest/syntsla/',
        RenegotiateSla: 'rest/renegotiate/'
    })
;
