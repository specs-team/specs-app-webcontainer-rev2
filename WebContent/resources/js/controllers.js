/**
 * This file contains the angular controller of the applications
 */

angular.module('SlaApp.controllers', [])

.controller('SlaCtrl', function ($scope, $location, $tabActive, $state) {

	$scope.tabActive = function(viewLocation){
		return $tabActive.get(viewLocation, $location.path());
	}

	$scope.go = function(route){
		$state.go(route);
	}

})

.controller('NegotiateCtrl', function ($scope, $location, $tabActive, $state) {

	$scope.tabActive = function(viewLocation){
		return $tabActive.get(viewLocation, $location.path());
	}

	// we will store all of our form data in this object
	$scope.formData = {
			capabilities: [{
				frameworks:[{
					securityControls: [{
						metrics: [{}]
					}]
				}]
			}]
	};

	$scope.formService = {};
	$scope.formCapabilities = {};
	$scope.formSecurities = {};
	$scope.formSecurityCtrl_SecurityControlWeight = {};
	$scope.formSecurityCtrl_DefaultSecurityControlWeight = {}; 
	$scope.formAgreements = {};
	$scope.formAgreementCtrl_Importance = {};
	$scope.formAgreementCtrl_Expression = {};
	$scope.formAgreementCtrl_DefaultImportance = {};
	$scope.formAgreementCtrl_DefaultExpression = {};
	$scope.formAgreementCtrl_Operand = {};
	$scope.formAgreementCtrl_SecurityControls = {};
	$scope.formOverview = {};
	$scope.formOverview_xml = {};
	$scope.evaluationTypes = [];
	
	$scope.routerState = $state;

	$scope.nextState = function(){

		switch($state.current.name) 
		{
		case 'negotiate.start':
			$state.go('negotiate.service');
			break;
		case 'negotiate.service':
			$state.go('negotiate.capability');
			break;
		case 'negotiate.capability':
			$state.go('negotiate.security');
			break;
		case 'negotiate.security':
			$state.go('negotiate.agreement');
			break;
		case 'negotiate.agreement':
			$state.go('negotiate.overview');
			break;
		default:
			$state.go('negotiate.start');
		}       
	}

	$scope.prevState = function(){

		switch($state.current.name) 
		{
		case 'negotiate.service':
			$state.go('negotiate.start');
			break;
		case 'negotiate.capability':
			$state.go('negotiate.service');
			break;
		case 'negotiate.security':
			$state.go('negotiate.capability');
			break;
		case 'negotiate.agreement':
			$state.go('negotiate.security');
			break;
		case 'negotiate.overview':
			$state.go('negotiate.agreement');
			break;
		default:
			$state.go('negotiate.start');
		} 

	}



})

.controller('SignCtrl', function ($scope, $location, $tabActive) {

	$scope.tabActive = function(viewLocation){
		return $tabActive.get(viewLocation, $location.path());
	}

	$scope.formSign = {};


})

.controller('ImplementCtrl', function ($scope, $location, $tabActive) {

	$scope.tabActive = function(viewLocation){
		return $tabActive.get(viewLocation, $location.path());
	}

	$scope.formImplement = {};


})

.controller('MonitorCtrl', function ($scope, $location, $tabActive, $state) {

	$scope.tabActive = function(viewLocation){
		return $tabActive.get(viewLocation, $location.path());
	}

	$scope.go = function(route){
		$state.go(route);
	}

	$scope.formMonitor = {};
	
})

;

//======================================================================================================================

angular.module('SlaApp.negotiate.controllers', [])

.controller('negotiate.StartCtrl', function ($scope, $location) {
	// we will store all of our form data in this object
	$scope.formData = {};
	$scope.formService = {};

})

.controller('negotiate.ServiceCtrl', function ($scope, $location, ServiceFactory, cfpLoadingBar, $tabActive) {

	//Set Negotiate Tab on the SLA NavBar
	$scope.$parent.tabActive = function(viewLocation){
		return $tabActive.set(viewLocation, '/negotiate/start');
	}

	$scope.alertMessage = "Loading . . .";
	cfpLoadingBar.start();
	ServiceFactory.all()
	.success(function (data, status, headers, config) {
		if(data.length == 0 || data.services.length == 0) {
			$scope.alertMessage = "No data received from server.";
			$scope.alertMessage = "";
		} else {
			$scope.services = data.services;
			$scope.formData.idsla = data.idsla;
			$scope.serviceArr = [];
			for(service in $scope.services)
			{
				var serviceObj = {};
				serviceObj.id = $scope.services[service].id;
				serviceObj.xml = "";
				serviceObj.idsla = $scope.idsla;
				$scope.serviceArr.push(serviceObj);
			}
		}
		cfpLoadingBar.complete();
	})
	.error(function (statusText) {
		$scope.errorState = true;
		$scope.errorMessage =  "Server Connection Error!";
		$scope.alertMessage = "";
		cfpLoadingBar.complete();
	});


	$scope.serviceRequested = false;

	$scope.showXMLServiceTemplate = function() {

		if($scope.formService.SDT_list != null)
		{
			if($scope.serviceArr[_.findIndex($scope.serviceArr, { id: $scope.formService.SDT_list.id })].xml == "")
			{
				cfpLoadingBar.start();
				var serviceIdentifier =  "/" + $scope.formService.SDT_list.id;
				ServiceFactory.get(serviceIdentifier)
				.success(function (data, status, headers, config) {
					cfpLoadingBar.complete();

					$scope.serviceRequested = true;
					$scope.formService_xml = LoadXMLString('XMLService',data);
					$scope.serviceArr[_.findIndex($scope.serviceArr, { id: $scope.formService.SDT_list.id })].xml = data;
				})
				.error(function (statusText) {
					$scope.errorState = true;
					$scope.errorMessage = "Server Connection Error!";
					cfpLoadingBar.complete();
				}); 
			}
			else{
				$scope.formService_xml = LoadXMLString('XMLService',$scope.serviceArr[_.findIndex($scope.serviceArr, { id: $scope.formService.SDT_list.id })].xml);
			}
		}

	}

	$scope.cleanXMLBox = function(){
		$scope.serviceRequested = false;
	}

})

.controller('negotiate.CapabilityCtrl', function ($scope, CapabilityFactory, cfpLoadingBar, $tabActive) {

	//Set Negotiate Tab on the SLA NavBar
	$scope.$parent.tabActive = function(viewLocation){
		return $tabActive.set(viewLocation, '/negotiate/start');
	}

	$scope.alertMessage = "Loading . . .";

	if($scope.formService.SDT_list != null)
	{
		cfpLoadingBar.start();
		CapabilityFactory.get($scope.formService.SDT_list.id)
		.success(function (data, status, headers, config) {
			if(data.length == 0) {
				$scope.alertMessage = "No data received from server."; 
				$scope.alertMessage = "";
			} else {
				$scope.capabilities = data.capabilities;
				$scope.idsla = data.idsla;
				$scope.formService.idsla = data.idsla;
				$scope.formData.idsla = data.idsla;
			}
			cfpLoadingBar.complete();
		})
		.error(function (statusText) {
			$scope.errorState = true;
			$scope.errorMessage = "Server Connection Error!";
			$scope.alertMessage = "";
			cfpLoadingBar.complete();
		});
	} else {
		$scope.alertMessage = "You did not select any service, therefore no control is displayed.";
	}

})

.controller('negotiate.SecurityCtrl', function ($scope,SecurityFactory, cfpLoadingBar, $tabActive) {

	//Set Negotiate Tab on the SLA NavBar
	$scope.$parent.tabActive = function(viewLocation){
		return $tabActive.set(viewLocation, '/negotiate/start');
	}

	$scope.alertMessage = "Loading . . .";
	fillCapabilities();

	if($scope.formData != null && $scope.formData.capabilities.length)
	{
		cfpLoadingBar.start();
		SecurityFactory.submit($scope.formData)
		.success(function (data, status, headers, config) {
			if(data.length == 0) {
				$scope.alertMessage = "No data received from server.";
				$scope.alertMessage = ""; 
			} else{
				$scope.capabilities = data.capabilities;
				$scope.idsla = data.idsla; 
			}
			cfpLoadingBar.complete();
		})
		.error(function (statusText) {
			$scope.errorState = true;
			$scope.errorMessage = "Server Connection Error!";

			$scope.alertMessage = "";
			cfpLoadingBar.complete();
		});

	} else {
		$scope.alertMessage = "You did not select any capability, therefore no control is displayed.";
	}

	function fillCapabilities() {

		var capabilities = [];
		for (item in $scope.formCapabilities) {
			if($scope.formCapabilities[item])
			{
				var tmpCap = {};
				tmpCap.id = item;
				capabilities.push(tmpCap);
			}
		}
		$scope.formData.capabilities = capabilities;
	}


})

.controller('negotiate.AgreementCtrl', function ($scope,AgreementFactory, cfpLoadingBar, $tabActive, $window, MetricService) {

	//Set Negotiate Tab on the SLA NavBar
	$scope.$parent.tabActive = function(viewLocation){
		return $tabActive.set(viewLocation, '/negotiate/start');
	}

	$scope.alertMessage = "Loading . . .";

	$scope.operators = ["equal", "less than", "greater than", "less or equal than", "greater or equal than"];

	if($scope.formData != null && $scope.formData.capabilities.length && fillSecurityControls()) {
		cfpLoadingBar.start();
		AgreementFactory.submit($scope.formData)
		.success(function (data, status, headers, config) {

			if(data.length == 0) {
				$scope.alertMessage = "No data received from server.";
				$scope.alertMessage = "";
			}
			else {
				$scope.capabilities = data.capabilities;
				$scope.idsla = data.idsla; 
			}
			cfpLoadingBar.complete();                         
		})
		.error(function (statusText) {
			$scope.errorState = true;
			$scope.errorMessage = "Server Connection Error!";
			$scope.alertMessage = "";
			cfpLoadingBar.complete();
		});

	} else {
		$scope.alertMessage = "You did not select any security control, therefore no control is displayed.";
	}

	function fillSecurityControls() {

		var result = false;
		var capabilities = [];
		var atleastOneSecurityControlSelected = false;

		for (item in $scope.formSecurities) {

			var tmpCap = {};
			tmpCap.id = item;
			tmpCap.frameworks = [];

			for (frm in $scope.formSecurities[item]){

				var tmpFrm = {};
				tmpFrm.id = frm;
				tmpCap.frameworks.push(tmpFrm);
				tmpCap.frameworks[tmpCap.frameworks.length-1].securityControls = [];

				for (sctrl in $scope.formSecurities[item][frm]){

					if ($scope.formSecurities[item][frm][sctrl]) 
					{
						atleastOneSecurityControlSelected = true;
						var tmpSctrl = {};
						tmpSctrl.id = sctrl;
						tmpSctrl.weight = $scope.formSecurityCtrl_SecurityControlWeight[sctrl];

						tmpCap.frameworks[tmpCap.frameworks.length-1].securityControls.push(tmpSctrl);            
					}
				}
			}

			if(atleastOneSecurityControlSelected)
				capabilities.push(tmpCap);
		}

		if(capabilities.length > 0){  
			$scope.formData.capabilities = capabilities;
			result = true; 
		}

		return result;
	}

	$scope.openMetricDetails = function(metricId){
		console.log("openMetricDetails called")
		AgreementFactory.detailendpoint()
		.success(function (data, status, headers, config) {
			console.log("Success occurred")
			if(data.length != 0) {

				$window.open(data.url + metricId);
			}
		})
		.error(function (statusText) {
			console.log("Error occurred")
		});
	}

	$scope.getDefaultOperatorName = function(value){

		return MetricService.getMetricOperator(value);
	}


})

.controller('negotiate.OverviewCtrl', function ($scope, OverviewFactory, _, $filter, cfpLoadingBar, $tabActive, $state, MetricService) {

	//Set Negotiate Tab on the SLA NavBar
	$scope.$parent.tabActive = function(viewLocation){
		return $tabActive.set(viewLocation, '/negotiate/start');
	}

	$scope.alertMessage = "Loading . . .";
	fillMetrics();

	if ($scope.processCompleted == true)
		$scope.alertMessage = "";
	else {
		$scope.alertMessage = "You did not select any control.";
	}


	$scope.offerSubmitted = false;
	$scope.offerSyntheticSubmitted = false;

	$scope.submitSLA = function() {

		if($scope.formData != null )
		{
			cfpLoadingBar.start();
			OverviewFactory.submit($scope.formData)
			.success(function (data, status, headers, config) {
				$scope.offers = data.offers;
				$scope.idsla = data.idsla;
				$scope.offerArr = [];
				$scope.offersEvaluation = [];
				for(offer in $scope.offers)
				{
					var offerObj = {};
					offerObj.id = $scope.offers[offer].id;
					offerObj.xml = "";
					offerObj.idsla = $scope.idsla;
					$scope.offerArr.push(offerObj);
					var offerEvaluation = {};
					offerEvaluation.id = $scope.offers[offer].id;
					offerEvaluation.evaluation = "loading";
					$scope.offersEvaluation.push(offerEvaluation);
				}
				cfpLoadingBar.complete();
				$scope.evaluateOffers();
			})
			.error(function (statusText) {
				$scope.errorState = true;
				$scope.errorMessage = "Server Connection Error!";
				cfpLoadingBar.complete();
			});      
		} 
	}
	
	$scope.evaluateOffers = function() {

		if($scope.offers != null )
		{
			cfpLoadingBar.start();
			OverviewFactory.evaluate($scope.offers, $scope.idsla)
			.success(function (data, status, headers, config) {
				$scope.offersEvaluation = data.offers;
				$scope.evaluationTypes = [{"type":"Root","name":"Root"},
				                          {"type":"Audit Assurance & Compliance","name":"AAC"},
				                          {"type":"Application & Interface Security","name":"AIS"},
				                          {"type":"Business Continuity Management & Operational Resilience","name":"BCR"},
				                          {"type":"Change Control & Configuration Management","name":"CCC"},
				                          {"type":"Datacenter Security","name":"DCS"},
				                          {"type":"Data Security & Information Lifecycle Management","name":"DSI"},
				                          {"type":"Encryption & Key Management","name":"EKM"},
				                          {"type":"Governance and Risk Management","name":"GRM"},
				                          {"type":"Human Resources","name":"HRS"},
				                          {"type":"Identity & Access Management","name":"IAM"},
				                          {"type":"Infrastructure & Virtualization Security","name":"IVS"},
				                          {"type":"Security Incident Management, E-Discovery & Cloud Forensics","name":"SEF"},
				                          {"type":"Supply Chain Management, Transparency and Accountability","name":"STA"}];
				$scope.evaluationType = {"type":"Root","name":"Root"};				
				
				cfpLoadingBar.complete();
			})
			.error(function (statusText) {
				$scope.errorState = true;
				$scope.errorMessage = "Server Connection Error!";
				cfpLoadingBar.complete();
			});      
		} 
	}
	
	$scope.rievaluateOffers = function() {

		if($scope.offersEvaluation != null )
		{
			cfpLoadingBar.start();
			OverviewFactory.rievaluate($scope.offersEvaluation, $scope.idsla, $scope.evaluationType.name)
			.success(function (data, status, headers, config) {
				$scope.offersEvaluation = data.offers;				
				
				cfpLoadingBar.complete();
			})
			.error(function (statusText) {
				$scope.errorState = true;
				$scope.errorMessage = "Server Connection Error!";
				cfpLoadingBar.complete();
			});      
		} 
	}


	function fillMetrics() {

		$scope.processCompleted = false;

		for (item in $scope.formAgreements) {

			var capabilityObj = new Object();
			capabilityObj.id = item;
			capabilityObj.frameworks = [];

			for (frm in $scope.formAgreements[item]){

				var frameworkObj = new Object();
				frameworkObj.id = frm;
				capabilityObj.frameworks.push(frameworkObj);
				capabilityObj.frameworks[capabilityObj.frameworks.length-1].metrics = [];

				for (mctrl in $scope.formAgreements[item][frm]){
					if ($scope.formAgreements[item][frm][mctrl]) {
						var metricObj = new Object();
						metricObj.id = mctrl;

						metricObj.importance = $scope.formAgreementCtrl_Importance[item][frm][mctrl];

						metricObj.operator = MetricService.getMetricOperator($scope.formAgreementCtrl_Expression[item][frm][mctrl]);

						metricObj.operand = $scope.formAgreementCtrl_Operand[item][frm][mctrl];

						metricObj.securityControls = $scope.formAgreementCtrl_SecurityControls[item][frm][mctrl];

						var targetMetricObj = new Object();
						targetMetricObj.id = metricObj.id;
						targetMetricObj.importance = metricObj.importance;
						targetMetricObj.operator = metricObj.operator;
						targetMetricObj.operand = metricObj.operand;

						for (secCtrlID in metricObj.securityControls) {

							var prop = 'id';
							var search_obj = {};
							search_obj[prop] = metricObj.securityControls[secCtrlID];
							var securityControlId = search_obj.id;
							$scope.formData = bindMetricToSecurityControl(securityControlId, targetMetricObj, $scope.formData);  

						}
					}
				}
			}
		}   
	}

	function bindMetricToSecurityControl(securityControlId, metricObj, targetObject) {

		for(capabilityIterator in targetObject.capabilities){
			for(frameworkIterator in targetObject.capabilities[capabilityIterator].frameworks){
				for(securityControlIterator in targetObject.capabilities[capabilityIterator].frameworks[frameworkIterator].securityControls){

					if(targetObject.capabilities[capabilityIterator].frameworks[frameworkIterator].securityControls[securityControlIterator].metrics == null)
						targetObject.capabilities[capabilityIterator].frameworks[frameworkIterator].securityControls[securityControlIterator].metrics = [];


					if(securityControlId == targetObject.capabilities[capabilityIterator].frameworks[frameworkIterator].securityControls[securityControlIterator].id){
						var len = targetObject.capabilities[capabilityIterator].frameworks[frameworkIterator].securityControls[securityControlIterator].metrics.length;

						if(len == 0 || !$filter('isIdExisting')(metricObj, targetObject.capabilities[capabilityIterator].frameworks[frameworkIterator].securityControls[securityControlIterator].metrics)){
							targetObject.capabilities[capabilityIterator].frameworks[frameworkIterator].securityControls[securityControlIterator].metrics.push(metricObj); 
						}
						if(targetObject.capabilities[capabilityIterator].frameworks[frameworkIterator].securityControls[securityControlIterator].metrics.length > 0)
							$scope.processCompleted = true;

						break;
					}
				}
			}  
		}

		return targetObject;
	} 

	$scope.submitOffer = function() {

		if($scope.formOverview.OfferList != null)
		{
			cfpLoadingBar.start();
			var offerObj = {};
			var index = _.findIndex($scope.offerArr, { id: $scope.formOverview.OfferList.id }); 
			if(index >= 0){
				offerObj = $scope.offerArr[index];
			}

			OverviewFactory.sendOffer(offerObj)
			.success(function (data, status, headers, config) {

				cfpLoadingBar.complete();
				$scope.offerSubmitted = true;
				goState('sign.start');
			})
			.error(function (data, status, headers, config) {
				$scope.errorState = true;
				$scope.errorMessage = "Server Connection Error!";

				cfpLoadingBar.complete();
			});      
		}
	}

	$scope.showXMLOffer = function() {

		if($scope.formOverview.OfferList != null)
		{
			if($scope.offerArr[_.findIndex($scope.offerArr, { id: $scope.formOverview.OfferList.id })].xml == "")
			{
				cfpLoadingBar.start();
				var offerIdentifier = $scope.idsla + "/" + $scope.formOverview.OfferList.id;
				OverviewFactory.get(offerIdentifier)
				.success(function (data, status, headers, config) {

					cfpLoadingBar.complete();

					$scope.offerSubmitted = true;
					$scope.offerSyntheticSubmitted = false;
					$scope.formOverview_xml = LoadXMLString('XMLOffer',data);
					$scope.offerArr[_.findIndex($scope.offerArr, { id: $scope.formOverview.OfferList.id })].xml = data;

				})
				.error(function (statusText) {
					$scope.errorState = true;
					$scope.errorMessage = "Server Connection Error!";
					// View error
					console.log("Errore di connessione al server: " + statusText);
					cfpLoadingBar.complete();
				}); 
			}
			else{
				$scope.offerSubmitted = true;
				$scope.offerSyntheticSubmitted = false;
				$scope.formOverview_xml = LoadXMLString('XMLOffer',$scope.offerArr[_.findIndex($scope.offerArr, { id: $scope.formOverview.OfferList.id })].xml);
			}
		}
	}
	
	$scope.showSyntheticOffer = function() {

		if($scope.formOverview.OfferList != null)
		{
				cfpLoadingBar.start();
				var offerIdentifier = $scope.idsla + "/" + $scope.formOverview.OfferList.id;
				OverviewFactory.getSynthetic(offerIdentifier)
				.success(function (data, status, headers, config) {

					cfpLoadingBar.complete();

					$scope.offerSubmitted = false;
					$scope.offerSyntheticSubmitted = true;
					
					$scope.synths = data.synth;
					$scope.provider = {"id":data.providerId,"name":data.providerName,"hardware":data.hardware};

				})
				.error(function (statusText) {
					$scope.errorState = true;
					$scope.errorMessage = "Server Connection Error!";
					// View error
					console.log("Errore di connessione al server: " + statusText);
					cfpLoadingBar.complete();
				}); 
		}
	}

	function goState(route){
		$state.go(route);
	} 


})

;

//======================================================================================================================

angular.module('SlaApp.sign.controllers', [])

.controller('sign.StartCtrl', function ($scope, $location, SignFactory, cfpLoadingBar, $state) {

	$scope.alertMessage = "Loading . . .";
	cfpLoadingBar.start();
	SignFactory.all()
	.success(function (data, status, headers, config) {

		if(data.length == 0 || data.slas.length == 0)
			$scope.alertMessage = "There are no SLAs to sign!";
		else {
			$scope.signs = data.slas;
			$scope.signArr = [];
			for(sign in $scope.signs)
			{
				var signObj = {};
				signObj.id = $scope.signs[sign].id;
				signObj.xml = "";
				$scope.signArr.push(signObj);
			}
		}
		cfpLoadingBar.complete();
	})
	.error(function (statusText) {
		$scope.errorState = true;
		$scope.errorMessage = "Server Connection Error!";
		// View error
		console.log("Errore di connessione al server: " + statusText);
		cfpLoadingBar.complete();
	});


	$scope.signSLA = function() {

		cfpLoadingBar.start();
		SignFactory.submit($scope.formSign.SlaList)
		.success(function (data, status, headers, config) {
			cfpLoadingBar.complete();
			goState('implement.start');
		})
		.error(function (statusText) {
			$scope.errorState = true;
			$scope.errorMessage = "Server Connection Error!";
			// View error
			console.log("Errore di connessione al server: " + statusText);
			cfpLoadingBar.complete();
		}); 
	}

	function goState(route){
		$state.go(route);
	} 

	$scope.templateRequested = false;

	$scope.showXMLSignTemplate = function() {
		$scope.syntView = false;
		$scope.templateRequested = true;
		if($scope.formSign.SlaList != null)
		{
			if($scope.signArr[_.findIndex($scope.signArr, { id: $scope.formSign.SlaList })].xml == "")
			{
				cfpLoadingBar.start();
				var signIdentifier = $scope.formSign.SlaList;
				SignFactory.get(signIdentifier)
				.success(function (data, status, headers, config) {
					cfpLoadingBar.complete();
					$scope.templateRequested = true;
					$scope.formSign_xml = LoadXMLString('XMLSign',data);
					$scope.signArr[_.findIndex($scope.signArr, { id: $scope.formSign.SlaList })].xml = data;
				})
				.error(function (statusText) {
					$scope.errorState = true;
					$scope.errorMessage = "Server Connection Error!";
					// View error
					console.log("Errore di connessione al server: " + statusText);
					cfpLoadingBar.complete();
				}); 
			}
			else{
				$scope.formSign_xml = LoadXMLString('XMLSign',$scope.signArr[_.findIndex($scope.signArr, { id: $scope.formSign.SlaList })].xml);
			}
		}

	}

	$scope.cleanSignBox = function(){
		$scope.templateRequested = false;
	}

	$scope.showSyntheticView = function(slaId) {
		$scope.cleanSignBox();
		SignFactory.getSynt(slaId)
		.success(function (data, status, headers, config) {
			$scope.syntView = true;
			//alert(data.synth);
			$scope.synths = data.synth;

		})
		.error(function (statusText) {

		}); 
	}

})

;

//======================================================================================================================

angular.module('SlaApp.implement.controllers', [])

.controller('implement.StartCtrl', function ($scope, $location, ImplementFactory, cfpLoadingBar, $state) {

	$scope.alertMessage = "Loading . . .";
	cfpLoadingBar.start();
	ImplementFactory.get()
	.success(function (data, status, headers, config) {

		if(data.length == 0 || data.slas.length == 0)
			$scope.alertMessageSigned = "There are no SLAs to implement!";
		else {
			$scope.implements = data.slas;
		}
		cfpLoadingBar.complete();
	})
	.error(function (statusText) {
		$scope.errorState = true;
		$scope.errorMessageSigned = "Server Connection Error!";
		// View error
		console.log("Errore di connessione al server: " + statusText);
		cfpLoadingBar.complete();
	});
	
	ImplementFactory.getWait()
	.success(function (data, status, headers, config) {

		if(data.length == 0 || data.slas.length == 0)
			$scope.alertMessageWait = "There are no SLAs to wait!";
		else {
			$scope.waitToSigns = data.slas;
		}
		cfpLoadingBar.complete();
	})
	.error(function (statusText) {
		$scope.errorState = true;
		$scope.errorMessageWait = "Server Connection Error!";
		// View error
		console.log("Errore di connessione al server: " + statusText);
		cfpLoadingBar.complete();
	});


	$scope.implementSLA = function() {

		cfpLoadingBar.start();
		ImplementFactory.submit($scope.formImplement.SlaList)
		.success(function (data, status, headers, config) {

			cfpLoadingBar.complete();
			if(data.state == "ERROR"){
				$scope.errorState = true;
				$scope.errorMessage = "Implementation Error!";
			}
			else{
				$state.go('monitor.start');   
			}
		})
		.error(function (response) {
			$scope.errorState = true;
			$scope.errorMessage = "Server Connection Error!";
			// View error
			console.log("Server Connection Error: " + response.statusText);
			
			alert("Error: "+response.status+"\nDescription: "+response.statusText);
			cfpLoadingBar.complete();
		}); 
	} 

})

//======================================================================================================================

angular.module('SlaApp.monitor.controllers', [])

.controller('monitor.StartCtrl',  function ($scope, $location, MonitorFactory, MonitorService, cfpLoadingBar, $state, $interval, $getDate, $window) {

	$scope.tooltipContent = "<div class=\"well\">In the following, the list of implemented SLAs is reported. <br/> For each SLA the current implementation status is displayed:<ol><li><b>CREATED:</b> the system has been initialized; </li> <li><b>BUILDING:</b> ...identifying the resources to acquire and the configurations to apply;</li> <li><b>IMPLEMENTING:</b> ...acquiring and configuring resources; </li> <li><b>ACTIVE:</b> the SLA has been implemented. Monitoring available;</li> <li><b>ERROR:</b> an error occurred. The SLA could not be implemented;</li></div>";
	$scope.annotationView = false;
	getMonitorList();
	var timer;
	startTimer();
	
	function startTimer(){
		timer = $interval(callAtInterval, 30000);
		console.log("Timer Started!");
	}

	function callAtInterval() {
		console.log("Call Interval occurred");
		getMonitorList();
	}

	function stopTimer(){
		$interval.cancel(timer);
		console.log("Timer Stopped!");
	}

	$scope.$on('$destroy', function(){
		stopTimer();
		console.log("Timer Stopped on destroy!");
	});


	function getMonitorList()
	{
		$scope.alertMessage = "Loading . . .";
		cfpLoadingBar.start();
		MonitorFactory.all()
		.success(function (data, status, headers, config) {
			
			if(data.length == 0 || data.implementations.length == 0)
				$scope.alertMessage = "There are no Implements to monitoring!";
			else {
				$scope.monitors = data.implementations;
				console.log($scope.monitors);
				$scope.monitorArr = [];
				for(monitor in $scope.monitors)
				{
					var monitorObj = {};
					monitorObj.id = $scope.monitors[monitor].id;
					monitorObj.xml = "";
					$scope.monitorArr.push(monitorObj);
					$scope.lastUpdate = $getDate.fromLong(new Date().getTime());
				}
			}
			cfpLoadingBar.complete();
		})
		.error(function (statusText) {
			$scope.errorState = true;
			$scope.errorMessage = "Server Connection Error!";
			// View error
			console.log("Errore di connessione al server: " + statusText);
			$scope.alertMessage = "";
			cfpLoadingBar.complete();
		});

	}

	$scope.getInfo = function(){
		stopTimer();
		getMonitorList();
		$scope.lastUpdate = $getDate.fromLong(new Date().getTime());
		startTimer();
	}

	$scope.templateRequested = false;

	$scope.showXMLMonitorTemplate = function(monitorId) {

		$scope.syntView = false;
		console.log(monitorId);
		if(monitorId != null)
		{
			cfpLoadingBar.start();
			$scope.monitorIdentifier = monitorId;
			MonitorFactory.get(monitorId)
			.success(function (data, status, headers, config) {

				cfpLoadingBar.complete();
				$scope.templateRequested = true;
				$scope.formMonitor_templateXml = LoadXMLString('XMLMonitor',data);
				$scope.monitorArr[_.findIndex($scope.monitorArr, { id: monitorId })].xml = data;
				$scope.signArr[_.findIndex($scope.signArr, { id: $scope.formSign.SlaList })].xml = data;
			})
			.error(function (statusText) {
				$scope.errorState = true;
				$scope.errorMessage = "Server Connection Error!";
				// View error
				console.log("Errore di connessione al server: " + statusText);
				cfpLoadingBar.complete();
			}); 

		}
	}

	$scope.cleanMonitorBox = function(){
		$scope.templateRequested = false;
	}

	$scope.goSummary = function(contextInfo){
		MonitorService.setContextInfo(contextInfo);
		$scope.$parent.go('monitor.summary');
	}
	
	$scope.startMonitorAction = function(planId, slaId){
		var e = document.getElementById(slaId+"_actionId");
		var selectedValue = e.options[e.selectedIndex].value;

		if(selectedValue == 'Manage'){
			$scope.gotoManage(planId);
		}else if(selectedValue == 'Renegotiate'){
			$scope.showRenegotiationView(slaId);
		}else if(selectedValue == 'Terminate'){
			$scope.terminate(slaId);
		}
	}

	$scope.gotoManage = function(planId) {
		MonitorFactory.plan(planId)
		.success(function (data, status, headers, config) {
			var form = document.createElement("form");
			form.action = data.url;
			form.method = 'POST';
			form.target = '_blank';

			var input = document.createElement("textarea");
			input.name = 'json';
			input.value = data.plan;
			form.appendChild(input);


			form.style.display = 'none';
			document.body.appendChild(form);
			form.submit();

		})
		.error(function (statusText) {

		}); 
	}

	$scope.showSynt = function(planId) {
		$scope.cleanMonitorBox();
		MonitorFactory.synt(planId)
		.success(function (data, status, headers, config) {
			$scope.syntView = true;
			//alert(data.synth);
			$scope.synths = data.synth;

		})
		.error(function (statusText) {

		}); 
	}
	
	$scope.showRenegotiationView = function(slaId) {
		$scope.cleanMonitorBox();
		MonitorFactory.getSynt(slaId)
		.success(function (data, status, headers, config) {
			$scope.renegotiationView = true;
			$scope.renegotiationSynths = data.synth;
			$scope.renegotiationSynthsSlaId = data.id;

		})
		.error(function (statusText) {

		}); 
	}
	
	$scope.startRenegotiation = function(slaId) {
		cfpLoadingBar.start();
		MonitorFactory.startRenegotiate(slaId, $scope.renegotiationSynths)
		.success(function (data, status, headers, config) {
			cfpLoadingBar.complete();
			alert("Response: "+status+"\nThe renegotiation is started correctly!");
			renegotiationView = false;
		})
		.error(function (data, status, headers, config) {
			cfpLoadingBar.complete();
			alert("Error: "+status+"\nAn error is occurred during the start of renegotiation!");
		}); 
	}

	$scope.terminate = function(slaId) {
		MonitorFactory.terminate(slaId)
		.success(function (data, status, headers, config) {

		})
		.error(function (statusText) {

		}); 
		$scope.getInfo();
	}

	$scope.annotations = function(slaId){
		console.log("Annotation " + slaId);
		MonitorFactory.annotation(slaId)
		.success(function (data, status, headers, config) {
			$scope.annotationsList = data;
			console.log("Response " + data.annotations.length);

		})
		.error(function (statusText) {

		}); 
		$scope.annotationView = true;

	}

	$scope.closeAnnotationView = function() {
		$scope.annotationView = false;
	}

})


.controller('monitor.SummaryCtrl', function ($scope, $location, SummaryFactory, MonitorService, cfpLoadingBar, $tabActive, $interval, $getDate) {

	//Set Monitor Tab on the SLA NavBar
	$scope.$parent.tabActive = function(viewLocation){
		return $tabActive.set(viewLocation, '/monitor/start');
	}
	var contextInfo = MonitorService.getContextInfo();
	$scope.planId = contextInfo.id;
	$scope.slaId = contextInfo.slaId;
	$scope.state = contextInfo.state;
	$scope.idimpl = contextInfo.idimpl;
	$scope.toggle = false;
	$scope.syntView = false;
	getMetricsDetails();
	var timer;
	startTimer();


	function startTimer(){
		timer = $interval(callAtInterval, 30000);
		console.log("Timer Summary Started!");
	}

	function callAtInterval() {
		console.log("Call Interval occurred");
		getMetricsDetails();
	}

	function stopTimer(){
		$interval.cancel(timer);
		console.log("Timer Summary Stopped!");
	}

	$scope.$on('$destroy', function(){
		stopTimer();
		console.log("Timer Summary Stopped on destroy!");
	});    

	function getMetricsDetails()
	{
		$scope.alertMessage = "Loading...";
		cfpLoadingBar.start();
		SummaryFactory.getMetricsDetails($scope.planId, $scope.slaId)
		.success(function (data, status, headers, config) {
			$scope.alertMessage = "No data received from server...";

			if(data.length == 0)
				$scope.alertMessage = "No SLOs were defined in the signed SLA. There are no metrics to monitor.";
			else {

				$scope.metricsdetails = data.metricsdetails;
				console.log($scope.metricsdetails);

				$scope.lastUpdate = $getDate.fromLong(new Date().getTime());

			}
			cfpLoadingBar.complete();
		})
		.error(function (statusText) {
			$scope.alertMessage = "No data received from server...";
			$scope.errorState = true;
			$scope.errorMessage = "Server Connection Error!";
			// View error
			console.log("Errore di connessione al server: " + statusText);
			cfpLoadingBar.complete();
		});

	}

	$scope.getInfo = function(){
		stopTimer();
		getMetricsDetails();
		$scope.lastUpdate = $getDate.fromLong(new Date().getTime());
		startTimer();
	}

	$scope.nodesDetails = function(){
		console.log($scope.toggle);

		if($scope.toggle){
			$scope.buttonLabel = "Hide Details";
			//Code for Show Node Details
			SummaryFactory.getNodes($scope.idimpl)
			.success(function (data, status, headers, config) {
				if(data.length == 0)
					$scope.alertMessage = "No SLOs were defined in the signed SLA. There are no metrics to monitor.";
				else {
					console.log(data);
					$scope.nodes = data.nodes;
					console.log($scope.nodes);

				}

			})
			.error(function (statusText) {
				$scope.errorState = true;
				$scope.errorMessage = "Server Connection Error!";

			});
		}
		else{
			$scope.buttonLabel = "Show Details";
			//Code for Hide Node Details
		}
		$scope.toggle = !$scope.toggle;
	}
})

;


