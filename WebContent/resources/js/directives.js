 angular.module('SlaApp.directives', [])
   
.directive('compNavbar', function(){
return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: true,
    templateUrl: 'resources/views/navbar.html',
    controller: function($scope, $element, $location){
        $scope.isActive = function(viewLocation){

            var active = false;

            if(viewLocation === $location.path()){
                active = true;
            }

            return active;

        }
    }
 }
})
 
.directive('slaTooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
          jq(element).popover({
            content: attrs.content,
            html: attrs.html,
            placement: attrs.position,
            trigger: 'hover'    
          })  
        }
    }
})
 
;