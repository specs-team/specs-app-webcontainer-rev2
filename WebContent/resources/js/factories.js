/**
 * This file contains the factory for every Module.
 * These functions call the rest Api of the MainRestController
 */

angular.module('SlaApp.negotiate.factories', [])

.factory('ServiceFactory', function ($http,NEGOTIATION_API) {
	return {
		all: function () {
            return $http.get(NEGOTIATION_API.Services, {
				timeout: 30000
			});
		},
        get: function (serviceIdentifier) {
			return $http.get(NEGOTIATION_API.ServiceXML + serviceIdentifier, {
				timeout: 30000
			})
		}
	}
})
.factory('CapabilityFactory', function ($http,NEGOTIATION_API) {
	return {
		all: function (serviceId) {
			return $http.get(NEGOTIATION_API.Capabilities, {
				timeout: 30000
			})
		},
        get: function (serviceId) {
			return $http.get(NEGOTIATION_API.Capabilities + serviceId, {
				timeout: 30000
			})
		}
	}
})
.factory('SecurityFactory', function ($http,NEGOTIATION_API) {
	return {
		submit: function (capabilitiesObj) {
			return $http.post(NEGOTIATION_API.Securities,capabilitiesObj,{timeout:30000})
		}
      
	}
})
.factory('AgreementFactory', function ($http,NEGOTIATION_API) {
	return {
		submit: function (securitiesObj) {
			 return $http.post(NEGOTIATION_API.Agreements,securitiesObj,{timeout:30000})
		},
		detailendpoint: function() {
			return $http.get(NEGOTIATION_API.Detail, {
					timeout: 30000
				})
		}
      
	}
})
.factory('OverviewFactory', function ($http,NEGOTIATION_API) {
	return {
		submit: function (agreementsObj) {
			 return $http.post(NEGOTIATION_API.Overview,agreementsObj,{timeout:30000})
		},
        get: function (offerIdentifier) {
			return $http.get(NEGOTIATION_API.OfferXML + offerIdentifier, {
				timeout: 30000
			})
		},
        getSynthetic: function (offerIdentifier) {
			return $http.get(NEGOTIATION_API.OfferSynthetic + offerIdentifier, {
				timeout: 30000
			})
		},
        sendOffer: function (offerObj) {
			 return $http.post(NEGOTIATION_API.Offer,offerObj,{timeout:30000})
		},
		evaluate: function (offersObj, slaId) {
			 return $http.post(NEGOTIATION_API.Evaluate+slaId,offersObj,{timeout:30000})
		},
		rievaluate: function (offersObj, slaId, queryParam) {
			 return $http.post(NEGOTIATION_API.Rievaluate+slaId+"/"+queryParam,offersObj,{timeout:30000})
		}
	}
})
;

//*******************************************************************************************

angular.module('SlaApp.sign.factories', [])

.factory('SignFactory', function ($http,SIGN_API) {
	return {
		all: function () {
            return $http.get(SIGN_API.Signs, {
				timeout: 30000
			});
		},
        get: function (signIdentifier) {
			return $http.get(SIGN_API.TemplateXML + signIdentifier, {
				timeout: 30000
			})
		},
        submit: function (signId) {
			 return $http.post(SIGN_API.Sign + signId, null,{timeout:30000})
		},
		getSynt: function (slaId) {
			return $http.get(SIGN_API.Synt + slaId, {
				timeout: 30000
			})
		}
	}
})
;

//*******************************************************************************************

angular.module('SlaApp.implement.factories', [])

.factory('ImplementFactory', function ($http,IMPLEMENT_API) {
	return {
		get: function () {
            return $http.get(IMPLEMENT_API.Implements, {
				timeout: 30000
			});
		},
		getWait: function () {
            return $http.get(IMPLEMENT_API.WaitToSign, {
				timeout: 30000
			});
		},
        submit: function (implementId) {
			 return $http.post(IMPLEMENT_API.Implement + implementId, null,{timeout:60000})
		}
	}
})
;

//*******************************************************************************************

angular.module('SlaApp.monitor.factories', [])

.factory('MonitorFactory', function ($http,MONITOR_API) {
	return {
        all: function () {
			return $http.get(MONITOR_API.Monitors, {
				timeout: 30000
			})
		},
        get: function (monitorIdentifier) {
			return $http.get(MONITOR_API.MonitorXML + monitorIdentifier, {
				timeout: 30000
			})
		},
		plan: function (planId) {
			return $http.get(MONITOR_API.Implemented + planId, {
					timeout: 30000
				})
		},
		synt: function (planId) {
			return $http.get(MONITOR_API.Synth + planId, {
					timeout: 30000
				})
		},
		getSynt: function (slaId) {
			return $http.get(MONITOR_API.SyntSla + slaId, {
				timeout: 30000
			})
		},
		terminate: function (slaId) {
			return $http.post(MONITOR_API.Terminate + slaId, {
					timeout: 30000
				})
		},
		annotation: function (slaId) {
			return $http.get(MONITOR_API.Annotation + slaId, {
					timeout: 30000
				})
		},
		startRenegotiate: function (slaId, renegotiationSynths) {
			return $http.post(MONITOR_API.RenegotiateSla + slaId, renegotiationSynths, {timeout: 30000})
		}
	}
})

.factory('SummaryFactory', function ($http,MONITOR_API) {
	return {
        getMetricsDetails: function (planId, slaId) {
            console.log(MONITOR_API.MetricsDetails + planId + "/" + slaId);
			return $http.get(MONITOR_API.MetricsDetails + planId + "/" + slaId, {
				timeout: 30000
			})
		},
		getNodes: function (implid) {
			return $http.get(MONITOR_API.Nodes + implid, {
				timeout: 30000
			})
		}
	}
})
;