angular.module('SlaApp.services', [])

  .factory('$tabActive', [function () {
    
      return {
        get: function (viewLocation, locationPath) {
          //console.log("VIEW LOCATION: " + viewLocation);
          //console.log("LOCATION PATH: " + locationPath);
          var active = false;
          if(viewLocation === locationPath){
            active = true;
          }
          return active;
		},
        set: function (viewLocation, locationTarget) {
          var active = false;
          if(viewLocation === locationTarget){
            active = true;
          }
			 return active;
		}
	}

  }])

  .factory('$getDate', [function () {
    
      return {
        fromLong: function (date) {
            var d = new Date(date);
            var myDate = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear()+" - "+ d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
            return myDate;
        }
      }

  }])

  .service('MetricService', function(NEGOTIATION_METRIC_DETAIL) {
    
    var getMetricOperator = function(value){
      
        if(value.length > 2){
          //Get Operator Value
          switch(value) 
          {
            case 'equal':
              return "eq";
              break;
            case 'less than':
              return "lt";
              break;
            case 'greater than':
              return "gt";
              break;
            case 'less or equal than':
              return "le";
              break;
            case 'greater or equal than':
              return "ge";
              break;
            default:
              return "";
          } 
        }
        else{
          //Get Operator Name
          switch(value) 
          {
            case 'eq':
              return "equal";
              break;
            case 'lt':
              return "less than";
              break;
            case 'gt':
              return "greater than";
              break;
            case 'le':
              return "less or equal than";
              break;
            case 'ge':
              return "greater or equal than";
              break;
          } 
        }
    };
  
    var getMetricDetailUrl = function(metricId){
      return NEGOTIATION_METRIC_DETAIL.Url + metricId;
      
    }

    return {
      getMetricOperator: getMetricOperator,
      getMetricDetailUrl: getMetricDetailUrl
    };

})

  .service('MonitorService', function() {
    var contextInfo = {};
    
    var getContextInfo = function(){
        return contextInfo;
    }
  
    var setContextInfo = function(value){
        contextInfo.id = value.id;
        contextInfo.slaId = value.slaid;
        contextInfo.state = value.state;
        contextInfo.idimpl = value.idimpl;
    }

    return {
      getContextInfo: getContextInfo,
      setContextInfo: setContextInfo
    };

})


;