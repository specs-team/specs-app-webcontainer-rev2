package specs.sw_application.frontend.entities;

//
//Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
//Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
//Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
//Generato il: 2016.04.18 alle 11:10:14 AM CEST 
//


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
* <p>Classe Java per anonymous complex type.
* 
* <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
* 
* <pre>
* &lt;complexType>
*   &lt;complexContent>
*     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*       &lt;sequence>
*         &lt;element name="annotation" maxOccurs="unbounded" minOccurs="0">
*           &lt;complexType>
*             &lt;complexContent>
*               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*                 &lt;sequence>
*                   &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
*                   &lt;element name="descr" type="{http://www.w3.org/2001/XMLSchema}byte"/>
*                   &lt;element name="timestamp" type="{http://www.w3.org/2001/XMLSchema}int"/>
*                 &lt;/sequence>
*               &lt;/restriction>
*             &lt;/complexContent>
*           &lt;/complexType>
*         &lt;/element>
*       &lt;/sequence>
*     &lt;/restriction>
*   &lt;/complexContent>
* &lt;/complexType>
* </pre>
* 
* 
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
 "annotation"
})
@XmlRootElement(name = "annotations")
public class Annotations {

 protected List<Annotations.Annotation> annotation;

 /**
  * Gets the value of the annotation property.
  * 
  * <p>
  * This accessor method returns a reference to the live list,
  * not a snapshot. Therefore any modification you make to the
  * returned list will be present inside the JAXB object.
  * This is why there is not a <CODE>set</CODE> method for the annotation property.
  * 
  * <p>
  * For example, to add a new item, do as follows:
  * <pre>
  *    getAnnotation().add(newItem);
  * </pre>
  * 
  * 
  * <p>
  * Objects of the following type(s) are allowed in the list
  * {@link Annotations.Annotation }
  * 
  * 
  */
 public List<Annotations.Annotation> getAnnotation() {
     if (annotation == null) {
         annotation = new ArrayList<Annotations.Annotation>();
     }
     return this.annotation;
 }


 /**
  * <p>Classe Java per anonymous complex type.
  * 
  * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
  * 
  * <pre>
  * &lt;complexType>
  *   &lt;complexContent>
  *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
  *       &lt;sequence>
  *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
  *         &lt;element name="descr" type="{http://www.w3.org/2001/XMLSchema}byte"/>
  *         &lt;element name="timestamp" type="{http://www.w3.org/2001/XMLSchema}int"/>
  *       &lt;/sequence>
  *     &lt;/restriction>
  *   &lt;/complexContent>
  * &lt;/complexType>
  * </pre>
  * 
  * 
  */
 @XmlAccessorType(XmlAccessType.FIELD)
 @XmlType(name = "", propOrder = {
     "id",
     "descr",
     "timestamp"
 })
 public static class Annotation {

     @XmlElement(required = true)
     protected String id;
     protected String descr;
     protected long timestamp;

     /**
      * Recupera il valore della proprietà id.
      * 
      * @return
      *     possible object is
      *     {@link String }
      *     
      */
     public String getId() {
         return id;
     }

     /**
      * Imposta il valore della proprietà id.
      * 
      * @param value
      *     allowed object is
      *     {@link String }
      *     
      */
     public void setId(String value) {
         this.id = value;
     }

     /**
      * Recupera il valore della proprietà descr.
      * 
      */
     public String getDescr() {
         return descr;
     }

     /**
      * Imposta il valore della proprietà descr.
      * 
      */
     public void setDescr(String value) {
         this.descr = value;
     }

     /**
      * Recupera il valore della proprietà timestamp.
      * 
      */
     public long getTimestamp() {
         return timestamp;
     }

     /**
      * Imposta il valore della proprietà timestamp.
      * 
      */
     public void setTimestamp(long value) {
         this.timestamp = value;
     }

 }

}
