package specs.sw_application.frontend.entities;

import java.util.List;

public class AnnotationsJson {
	private List<AnnotationJson> annotations;
	private String slaId;
	
	public AnnotationsJson (String slaId, List<AnnotationJson> annotations){
		this.slaId = slaId;
		this.annotations = annotations;
	}
	
	public List<AnnotationJson> getAnnotations() {
		return annotations;
	}
	public void setAnnotations(List<AnnotationJson> annotations) {
		this.annotations = annotations;
	}
	public String getSlaId() {
		return slaId;
	}
	public void setSlaId(String slaId) {
		this.slaId = slaId;
	}

}
