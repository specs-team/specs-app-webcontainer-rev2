package specs.sw_application.frontend.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChoosedCapabilities {
	@JsonProperty("capabilities")
	public List<IdList>capabilities;
	@JsonProperty("idsla")
	public String idsla;
	
	public ChoosedCapabilities() {
		setIdsla(null);
		setCapabilities(null);
	}
	
	public String getIdsla() {
		return idsla;
	}
	public void setIdsla(String idsla) {
		this.idsla = idsla;
	}
	public List<IdList> getCapabilities() {
		return capabilities;
	}
	public void setCapabilities(List<IdList> capabilities) {
		this.capabilities = capabilities;
	}
	
}
