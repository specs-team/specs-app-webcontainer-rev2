package specs.sw_application.frontend.entities;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CollectionTypeJson implements Serializable{

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("resource")
    private String name;
    @JsonProperty("total")
    private int total;
    @JsonProperty("members")
    private int members;

    @JsonProperty("item_list")
    private List<ItemJson> itemList;

   
    public static class ItemJson implements Serializable{

        @JsonProperty("id")
        private String id;
        @JsonProperty("item")
        private String item;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getItem() {
			return item;
		}
		public void setItem(String item) {
			this.item = item;
		}
       

    }


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getTotal() {
		return total;
	}


	public void setTotal(int total) {
		this.total = total;
	}


	public int getMembers() {
		return members;
	}


	public void setMembers(int members) {
		this.members = members;
	}


	public List<ItemJson> getItemList() {
		return itemList;
	}


	public void setItemList(List<ItemJson> itemList) {
		this.itemList = itemList;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
