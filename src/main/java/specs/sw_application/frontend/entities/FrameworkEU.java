package specs.sw_application.frontend.entities;

import java.util.List;

public class FrameworkEU {
	private String id;
	private String name;
	private List<SecurityControlEU> securityControls;
	
	public FrameworkEU(String id, String frameworkName,
			List<SecurityControlEU> securityControl) {
		setId(id);
		setName(frameworkName);
		setSecurityControls(securityControl);
	}
	
	public FrameworkEU() {
		setId(null);
		setName(null);
		setSecurityControls(null);
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<SecurityControlEU> getSecurityControls() {
		return securityControls;
	}
	public void setSecurityControls(List<SecurityControlEU> securityControl) {
		this.securityControls = securityControl;
	}
	
}
