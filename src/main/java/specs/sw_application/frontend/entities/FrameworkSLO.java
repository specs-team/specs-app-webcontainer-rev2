package specs.sw_application.frontend.entities;

import java.util.ArrayList;
import java.util.List;

public class FrameworkSLO {
	String id;
	List<MetricEU> metrics;
	
	public FrameworkSLO(String id, List<MetricEU> metrics) {
		setId(id);
		setMetrics(metrics);
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<MetricEU> getMetrics() {
		return metrics;
	}
	public void setMetrics(List<MetricEU> metrics) {
		this.metrics = metrics;
	}
}
