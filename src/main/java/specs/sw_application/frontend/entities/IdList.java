package specs.sw_application.frontend.entities;


import com.fasterxml.jackson.annotation.JsonProperty;

public class IdList {
	@JsonProperty("id")
	public String id;
	
	public IdList() {
		this.id = "";
	}
	
	public IdList(String ids) {
		this.id = ids;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String ids) {
		this.id = ids;
	}
}
