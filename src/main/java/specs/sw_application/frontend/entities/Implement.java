package specs.sw_application.frontend.entities;

public class Implement {
	private String id;
	private String slaid;
	private String state;
	private String idimpl;
	private String slastate;
	
	public Implement(String id, String slaid, String state, String idimpl) {
		setId(id);
		setState(state);
		setSlaid(slaid);
		setIdimpl(idimpl);
		setSlastate("UNKNOWN");
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSlaid() {
		return slaid;
	}
	public void setSlaid(String slaid) {
		this.slaid = slaid;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	public String getIdimpl() {
		return idimpl;
	}

	public void setIdimpl(String idimpl) {
		this.idimpl = idimpl;
	}

	public String getSlastate() {
		return slastate;
	}

	public void setSlastate(String slastate) {
		this.slastate = slastate;
	}
}
