package specs.sw_application.frontend.entities;

import java.util.ArrayList;
import java.util.List;

public class MetricEU {
	private String id;
	private String name;
	private String description;
	private String importance;
	private String operator;
	private String operand;
	private List<String> securityControls;
	private List<String> importances;
	
	public MetricEU(String id, String name, String description, String importance,
			String operator, String operand, List<String> securityControls) {
		setId(id);
		setName(name);
		setDescription(description);
		setImportance(importance);
		setOperand(operand);
		setOperator(operator);
		setSecurityControls(securityControls);
		List<String> defaultImportances = new ArrayList<String>();
		defaultImportances.add("LOW");
		defaultImportances.add("MEDIUM");
		defaultImportances.add("HIGH");
		setImportances(defaultImportances);
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImportance() {
		return importance;
	}
	public void setImportance(String importance) {
		this.importance = importance;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getOperand() {
		return operand;
	}
	public void setOperand(String operand) {
		this.operand = operand;
	}
	public List<String> getSecurityControls() {
		return securityControls;
	}
	public void setSecurityControls(List<String> securityControls) {
		this.securityControls = securityControls;
	}
	public List<String> getImportances() {
		return importances;
	}
	public void setImportances(List<String> weights) {
		this.importances = weights;
	}
}
