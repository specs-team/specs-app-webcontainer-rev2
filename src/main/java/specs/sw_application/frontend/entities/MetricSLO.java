package specs.sw_application.frontend.entities;

public class MetricSLO {
	private String id;
	private String importance;
	private String operator;
	private String operand;
	
	public MetricSLO(String id, String importance, String operator, String operand) {
		setId(id);
		setImportance(importance);
		setOperand(operand);
		setOperator(operator);
	}
	
	public MetricSLO() {
		// For json mapping
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImportance() {
		return importance;
	}
	public void setImportance(String importance) {
		this.importance = importance;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getOperand() {
		return operand;
	}
	public void setOperand(String operand) {
		this.operand = operand;
	}

}
