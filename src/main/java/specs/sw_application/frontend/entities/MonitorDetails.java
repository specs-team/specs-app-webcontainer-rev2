package specs.sw_application.frontend.entities;

import java.util.List;

public class MonitorDetails {
	private List<Metricsdetail> metricsdetails;

    public MonitorDetails(List<Metricsdetail> metricsdetails){
        this.setMetricsdetails(metricsdetails);
    }

    public MonitorDetails(){
        this.setMetricsdetails(null);
    }
    
    public List<Metricsdetail> getMetricsdetails() {
		return metricsdetails;
	}

	public void setMetricsdetails(List<Metricsdetail> metricsdetails) {
		this.metricsdetails = metricsdetails;
	}

	public static final class Metricsdetail {
        private  String capabilityname;
        private  String metricname;
        private  String sloOperator;
        private  String sloOperand;
        private  String measuredvalue;
        private  String timestamp;
        private  String state;

        public Metricsdetail(String capabilityname, String metricname, String sloOperator, String sloOperand, String measuredvalue, String timestamp, String state) {
        	setCapabilityname(capabilityname);
        	setMetricname(metricname);
            setSloOperator(sloOperator);
            setSloOperand(sloOperand);
            setMeasuredvalue(measuredvalue);
            setTimestamp(timestamp);
            setState(state);
        }

        public Metricsdetail() {
        	// For json mapping
        }
        
        public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}
		
		public String getCapabilityname() {
			return capabilityname;
		}

		public void setCapabilityname(String capabilityname) {
			this.capabilityname = capabilityname;
		}

		public String getMetricname() {
			return metricname;
		}

		public void setMetricname(String metricname) {
			this.metricname = metricname;
		}

		public String getSloOperator() {
			return sloOperator;
		}

		public void setSloOperator(String sloOperator) {
			this.sloOperator = sloOperator;
		}
		
		public String getSloOperand() {
			return sloOperand;
		}

		public void setSloOperand(String sloOperand) {
			this.sloOperand = sloOperand;
		}

		public String getMeasuredvalue() {
			return measuredvalue;
		}

		public void setMeasuredvalue(String measuredvalue) {
			this.measuredvalue = measuredvalue;
		}

		public String getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(String timestamp) {
			this.timestamp = timestamp;
		}
		
    }
}
