package specs.sw_application.frontend.entities;

import java.util.List;

public final class NodesInfo {
    public final List<Node> nodes;

    public NodesInfo(List<Node> nodes){
        this.nodes = nodes;
    }

    public static final class Node {
        public final String name;
        public final String publicip;
        public final List<String> privateip;
        public final List<String> securities;

        public Node(String name, String publicip, List<String> privateip, List<String> securities){
            this.name = name;
            this.publicip = publicip;
            this.privateip = privateip;
            this.securities = securities;
        }
    }
}