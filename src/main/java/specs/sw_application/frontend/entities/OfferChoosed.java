package specs.sw_application.frontend.entities;

public class OfferChoosed {
	private String id;
	private String idsla;
	private String xml;
	
	public OfferChoosed(String id, String idsla, String xml) {
		setId(id);
		setIdsla(idsla);
		setXml(xml);
	}
	
	public OfferChoosed() {
		setId(null);
		setIdsla(null);
		setXml(null);
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdsla() {
		return idsla;
	}
	public void setIdsla(String idsla) {
		this.idsla = idsla;
	}
	public String getXml() {
		return xml;
	}
	public void setXml(String xml) {
		this.xml = xml;
	}
}
