package specs.sw_application.frontend.entities;

public class OfferEvaluation {
	private String id;
	private String idReasoner;
	private String evaluation;
	
	public OfferEvaluation(String id, String idReasoner, String evaluation) {
		setId(id);
		setEvaluation(evaluation);
		setIdReasoner(idReasoner);
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdReasoner() {
		return idReasoner;
	}
	public void setIdReasoner(String idReasoner) {
		this.idReasoner = idReasoner;
	}
	public String getEvaluation() {
		return evaluation;
	}
	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}
}
