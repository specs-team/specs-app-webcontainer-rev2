package specs.sw_application.frontend.entities;

import java.util.List;

public class OffersEvaluation {
	private String idsla;
	private List<OfferEvaluation> offers;
	
	public OffersEvaluation(String idsla, List<OfferEvaluation> offers) {
		setIdsla(idsla);
		setOffers(offers);
	}

	public String getIdsla() {
		return idsla;
	}

	public void setIdsla(String idsla) {
		this.idsla = idsla;
	}

	public List<OfferEvaluation> getOffers() {
		return offers;
	}

	public void setOffers(List<OfferEvaluation> offers) {
		this.offers = offers;
	}
	
	
	
}
