package specs.sw_application.frontend.entities;

import java.util.List;

public class OffersInfo {
	private String idsla;
	private List<OfferInfo> offers;
	
	public OffersInfo(String idsla, List<OfferInfo> offers) {
		setIdsla(idsla);
		setOffers(offers);
	}

	public String getIdsla() {
		return idsla;
	}

	public void setIdsla(String idsla) {
		this.idsla = idsla;
	}

	public List<OfferInfo> getOffers() {
		return offers;
	}

	public void setOffers(List<OfferInfo> offers) {
		this.offers = offers;
	}
	
	
	
}
