package specs.sw_application.frontend.entities;

import java.util.List;

public class PlanImplementations {
	private List<Implement> implementations;

	
	public PlanImplementations(List<Implement> impls) {
		setImplementations(impls);
	}
	
	public List<Implement> getImplementations() {
		return implementations;
	}

	public void setImplementations(List<Implement> implementations) {
		this.implementations = implementations;
	}
	

}
