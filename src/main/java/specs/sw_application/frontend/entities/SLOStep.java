package specs.sw_application.frontend.entities;

import java.util.List;

public class SLOStep {
	String idsla;
	List<CapabilitySLOStep> capabilities;
	
	public SLOStep(String idsla, List<CapabilitySLOStep> capabilities) {
		setIdsla(idsla);
		setCapabilities(capabilities);
	}
	
	public String getIdsla() {
		return idsla;
	}
	public void setIdsla(String idsla) {
		this.idsla = idsla;
	}
	public List<CapabilitySLOStep> getCapabilities() {
		return capabilities;
	}
	public void setCapabilities(List<CapabilitySLOStep> capabilities) {
		this.capabilities = capabilities;
	}
}
