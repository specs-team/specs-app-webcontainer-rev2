package specs.sw_application.frontend.entities;

public class SlaId {
	private String sla_id;
	
	public SlaId() {
		this.sla_id = "";
	}
	
	public SlaId(String id) {
		this.setSla_id(id);
	}

	public String getSla_id() {
		return sla_id;
	}

	public void setSla_id(String sla_id) {
		this.sla_id = sla_id;
	}
}
