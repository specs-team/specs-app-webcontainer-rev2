package specs.sw_application.frontend.entities;

import java.util.ArrayList;
import java.util.List;

public class SlaIds {

	private List<IdList> slas;
	
	public SlaIds() {
		this.slas = new ArrayList<IdList>();
	}
	
	public SlaIds(List<IdList> slas) {
		this.setSlas(slas);
	}

	public List<IdList> getSlas() {
		return slas;
	}

	public void setSlas(List<IdList> slas) {
		this.slas = slas;
	}
	
}
