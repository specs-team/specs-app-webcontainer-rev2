package specs.sw_application.frontend.entities;

import java.util.List;


public final class Synthetic {
    public final String id;
    public final List<Synth> synth;

    public Synthetic(List<Synth> synth, String id){
        this.synth = synth;
        this.id = id;
    }

    public static final class Synth {
        public final String name;
        public final String operation;
        public final String value;
        public final String importance;

        public Synth(String name, String operation, String value, String importance){
            this.name = name;
            this.operation = operation;
            this.value = value;
            this.importance = importance;
        }
    }
}