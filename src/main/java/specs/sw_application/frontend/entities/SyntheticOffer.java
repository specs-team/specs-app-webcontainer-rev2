package specs.sw_application.frontend.entities;

import java.util.List;

import specs.sw_application.frontend.entities.Synthetic.Synth;


public final class SyntheticOffer {
    public final List<Synth> synth;
    public final String providerId;
    public final String providerName;
    public final String hardware;

    public SyntheticOffer(List<Synth> synth, String providerId, String providerName, String hardware){
        this.synth = synth;
        this.providerId = providerId;
        this.providerName = providerName;
        this.hardware = hardware;
    }
}