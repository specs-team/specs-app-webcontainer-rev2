package specs.sws_application.frontend.mvc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;

import com.google.gson.Gson;

import specs.sw_application.frontend.entities.UserProfile;
import specs.sws_application.utility.MyHttpServletRequestWrapper;
import specs.sws_application.utility.PropertiesManager;

@Scope("session")
public class RequestsFilter implements Filter {

	@Override
	public void destroy() {
		// ...
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, 
			ServletResponse response, FilterChain chain)
					throws IOException, ServletException {
		System.out.println("doFilter CALLED: "+((HttpServletRequest)request).getRequestURL());
		HttpSession session = ((HttpServletRequest)request).getSession(false);

		Object sessionToken = session != null ? session.getAttribute("token") : null;
		Object parameterToken = request.getParameter("token");

		if(parameterToken != null){
			System.out.println("PARAMETER TOKEN IS NOT NULL");

			String req_uri=((HttpServletRequest)request).getRequestURI();
			HttpURLConnection httpURLConnection = doGetAccessRequest(
					PropertiesManager.getProperty("access_verify.endpoint")+"?path_res="+req_uri+"&action=view", parameterToken.toString());
			String access_response = parseAccessResponse(httpURLConnection);
			if(access_response.equals("PERMIT")){
				HttpSession sessionNew = ((HttpServletRequest)request).getSession();
				sessionNew.setAttribute("token", parameterToken.toString());

		        HttpServletRequest newRequest = new MyHttpServletRequestWrapper(((HttpServletRequest)request));

				if(sessionToken != null && session.getAttribute("profile_username") != null){
					chain.doFilter(newRequest, response);
				}else{
					HttpURLConnection httpURLConnectionProfile = doGetAccessRequest(
							PropertiesManager.getProperty("profile.endpoint"), parameterToken.toString());
					String profile = parseResponse(httpURLConnectionProfile);
					UserProfile userProfile = new Gson().fromJson(profile, UserProfile.class);
					sessionNew.setAttribute("profile_username", userProfile.getUsername());
					sessionNew.setAttribute("profile_role", userProfile.getRole());
					chain.doFilter(newRequest, response);
				}
			}else{
				System.out.println("NO PERMIT: "+access_response);
				request.setAttribute("error", "Forbidden");
				request.setAttribute("error_description", "The requested resource is not accessible! You are not authorized!");
				request.getRequestDispatcher("/redirect_error.jsp").forward(request, response);
			}
		}else if(sessionToken != null){
			System.out.println("SESSION TOKEN IS NOT NULL");

			String req_uri=((HttpServletRequest)request).getRequestURI();
			HttpURLConnection httpURLConnection = doGetAccessRequest(
					PropertiesManager.getProperty("access_verify.endpoint")+"?path_res="+req_uri+"&action=view",sessionToken.toString());
			String access_response = parseAccessResponse(httpURLConnection);
			if(access_response.equals("PERMIT")){
				System.out.println("PERMIT");

				if(session.getAttribute("profile_username") == null){
					HttpURLConnection httpURLConnectionProfile = doGetAccessRequest(
							PropertiesManager.getProperty("profile.endpoint"), sessionToken.toString());
					String profile = parseResponse(httpURLConnectionProfile);
					UserProfile userProfile = new Gson().fromJson(profile, UserProfile.class);
					session.setAttribute("profile_username", userProfile.getUsername());
					session.setAttribute("profile_role", userProfile.getRole());
				}
				chain.doFilter(request, response);
			}else{
				System.out.println("NO PERMIT: "+access_response);
				request.setAttribute("error", "Forbidden");
				request.setAttribute("error_description", "The requested resource is not accessible! You are not authorized!");
				request.getRequestDispatcher("/redirect_error.jsp").forward(request, response);
			}
		}else{
			request.setAttribute("error", "Session not valid!");
			request.setAttribute("error_description", "No session is found, try to execute the login!");
			request.getRequestDispatcher("/redirect_error.jsp").forward(request, response);
		}
	}

	private HttpURLConnection doGetAccessRequest(String urlPath, String token) throws IOException {
		URL url = new URL(urlPath);
		URLConnection c = url.openConnection();

		if (c instanceof HttpURLConnection) {
			HttpURLConnection httpURLConnection = (HttpURLConnection)c;
			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setAllowUserInteraction(false);
			httpURLConnection.setRequestProperty("Authorization", "bearer "+token);
			httpURLConnection.connect();

			return httpURLConnection;
		}
		return null;
	}

	public static String parseAccessResponse(HttpURLConnection httpURLConnection) throws IOException{
		int responseCode = httpURLConnection.getResponseCode();
		System.out.println("RESPONSE CODE: "+responseCode);
		if(responseCode == 200){
			BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
			StringBuilder sb = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			String access_response = sb.toString();

			System.out.println("ACCESS RESPONSE: "+access_response);
			return access_response;
		}else{
			BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getErrorStream())));
			StringBuilder sb = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			String access_response = sb.toString();

			System.out.println("ACCESS RESPONSE: "+access_response);
			return access_response;
		}
	}

	public static String parseResponse(HttpURLConnection httpURLConnection) throws IOException{
		int responseCode = httpURLConnection.getResponseCode();
		System.out.println("RESPONSE CODE: "+responseCode);
		if(responseCode == 200){
			BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
			StringBuilder sb = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			String profile = sb.toString();

			System.out.println("PROFILE RESPONSE: "+profile);
			return profile;
		}else{
			BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getErrorStream())));
			StringBuilder sb = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			String profile = sb.toString();

			System.out.println("PROFILE RESPONSE: "+profile);
			return profile;
		}	
	}

}