package specs.sws_application.frontend.mvc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import specs.sws_application.utility.PropertiesManager;

@Controller
public class WebController {
	@RequestMapping("/index")
    public ModelAndView helloWorld(Model model) {
        model.addAttribute("message", "Hello World!");
        return new ModelAndView("index", "message", model);
    }
	
	@RequestMapping(value="/redirect_error",method=RequestMethod.GET)
	public ModelAndView redirectErrorReq(@Context HttpServletRequest request, @Context HttpServletResponse response) throws ServletException, IOException{
		System.out.println("Redirect error called");
		
		request.setAttribute("platform_path", PropertiesManager.getProperty("platform_interface.basepath")+"/platform-interface");
		return new ModelAndView("redirect_error");
	}
	
	@RequestMapping(value="/logout",method=RequestMethod.GET)
	public String logout(@Context HttpServletRequest request) {
		System.out.println("LOGOUT CALLED");
		
		Object token = request.getSession().getAttribute("token");
		if(token != null){
			HttpURLConnection httpURLConnection;
			try {
				httpURLConnection = doGetWithToken2Request(PropertiesManager.getProperty("revoke_token.endpoint"), token.toString());
				System.out.println("Token revoke response code: "+httpURLConnection.getResponseCode());
				
				BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
				StringBuilder sb = new StringBuilder();
				String output;
				while ((output = br.readLine()) != null) {
					sb.append(output);
				}
				String response = sb.toString();

				System.out.println("Token revoke response body: "+response);

				request.getSession().setAttribute("token", null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return ("redirect:"+PropertiesManager.getProperty("oauth_server_index.endpoint")+"?redirect_url="+PropertiesManager.getProperty("platform_interface.basepath")+"/platform-interface");
	}
	
	public static HttpURLConnection doGetWithToken2Request(String urlPath, String token) throws IOException {
		URL url = new URL(urlPath);
		URLConnection c = url.openConnection();

		if (c instanceof HttpURLConnection) {
			HttpURLConnection httpURLConnection = (HttpURLConnection)c;
			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setAllowUserInteraction(false);
			httpURLConnection.setRequestProperty("Authorization", "bearer "+token);
			httpURLConnection.connect();

			return httpURLConnection;
		}
		return null;
	}
}
