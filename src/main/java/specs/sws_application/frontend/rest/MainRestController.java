package specs.sws_application.frontend.rest;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import specs.sw_application.frontend.entities.AnnotationJson;
import specs.sw_application.frontend.entities.Annotations;
import specs.sw_application.frontend.entities.AnnotationsJson;
import specs.sw_application.frontend.entities.CapabilitiesEU;
import specs.sw_application.frontend.entities.ChoosedCapabilities;
import specs.sw_application.frontend.entities.CollectionType;
import specs.sw_application.frontend.entities.CollectionType.ItemList;
import specs.sw_application.frontend.entities.CollectionTypeJson;
import specs.sw_application.frontend.entities.CollectionTypeJson.ItemJson;
import specs.sw_application.frontend.entities.CollectionTypeTemp;
import specs.sw_application.frontend.entities.CollectionTypeTemp.Item;
import specs.sw_application.frontend.entities.IdList;
import specs.sw_application.frontend.entities.Implement;
import specs.sw_application.frontend.entities.PlanImplementations;
import specs.sw_application.frontend.entities.MonitorDetails;
import specs.sw_application.frontend.entities.MonitorDetails.Metricsdetail;
import specs.sw_application.frontend.entities.NodesInfo;
import specs.sw_application.frontend.entities.NodesInfo.Node;
import specs.sw_application.frontend.entities.OfferChoosed;
import specs.sw_application.frontend.entities.OfferEvaluation;
import specs.sw_application.frontend.entities.OfferInfo;
import specs.sw_application.frontend.entities.OffersEvaluation;
import specs.sw_application.frontend.entities.OffersInfo;
import specs.sw_application.frontend.entities.SLOStep;
import specs.sw_application.frontend.entities.ServiceEU;
import specs.sw_application.frontend.entities.ServicesEU;
import specs.sw_application.frontend.entities.SlaIds;
import specs.sw_application.frontend.entities.Synthetic;
import specs.sw_application.frontend.entities.Synthetic.Synth;
import specs.sw_application.frontend.entities.SyntheticOffer;
import specs.sws_application.utility.BuilderExtractor;
import specs.sws_application.utility.PropertiesManager;
import specs.sws_application.utility.Utility;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.datamodel.enforcement.ImplementationPlan.Pool;
import eu.specs.datamodel.enforcement.ImplementationPlan.Vm;
import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.datamodel.enforcement.SupplyChain.Allocation;
import eu.specs.datamodel.sla.sdt.SLOType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType.ServiceResources;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType.ServiceResources.ResourcesProvider;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType.ServiceResources.ResourcesProvider.VM;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hamcrest.core.IsInstanceOf;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@RestController
@RequestMapping("/")
@Configuration
@PropertySource("classpath:specs_app.properties")
@Scope("session")
public class MainRestController {
	@Autowired
	Environment env;
	private static final Logger logger = LogManager
			.getLogger(MainRestController.class);

	BuilderExtractor builder = new BuilderExtractor();
	//	Properties properties = null;


	private final RestTemplate rest = new RestTemplate();

	public MainRestController() {

		//		setProperties("file:"+System.getenv("SPECSAPPCONFPATH"));
	}

	public RestTemplate getRestTemplate() {
		return rest;
	}

	//	public void setProperties(String url) {
	//		ResourceLoader resourceLoader = new DefaultResourceLoader();
	//		Resource resource = resourceLoader.getResource(url);
	//		if (!resource.exists()) {
	//			resource = resourceLoader.getResource("classpath:specs_app.properties");
	//		}
	//
	//		properties = new Properties();
	//		try {
	//			properties.load(resource.getInputStream());
	//		} catch (IOException e) {
	//			logger.info(e);
	//		}
	//	}

	/**
	 * Retrieve the capabilities of service
	 * 
	 * @param service
	 *            - Service's name
	 * @return The capabilities
	 */

	@RequestMapping(value = "/capabilities/{service}", method = RequestMethod.GET)
	public ResponseEntity<CapabilitiesEU> capabilities(
			@Context HttpServletRequest request, @PathVariable(value = "service") String service) {
		AgreementOffer offer = null;
		System.out.println("Profile username from session: "+request.getSession().getAttribute("profile_username"));
		offer = builder.getTemplateFromService(service, request);

		HttpHeaders responseHeaders = new HttpHeaders();

		return new ResponseEntity<CapabilitiesEU>(
				builder.getAllCapabilities(offer), responseHeaders,
				HttpStatus.OK);
	}

	/**
	 * Retrieve the list of the services
	 * 
	 * @return the list of the services
	 */

	@RequestMapping(value = "/services", method = RequestMethod.GET)
	public ResponseEntity<ServicesEU> services() {
		List<ServiceEU> list = new ArrayList<ServiceEU>();
		try {
			CollectionType servers = (CollectionType) rest.getForObject(
					PropertiesManager.getProperty("sla_template.endpoint"),
					CollectionType.class);

			builder.setCloudSlaEndpoin(PropertiesManager
					.getProperty("cloud_sla.endpoint"));
			for (int i = 0; i < servers.getItemList().size(); i++) {
				String id = Utility.lastElement(servers.getItemList().get(i)
						.getValue(), "/");
				ServiceEU service = new ServiceEU(id);
				list.add(service);

				builder.putServiceUrl(id, servers.getItemList().get(i)
						.getValue());
			}
		} catch (Exception e) {
			logger.info(e);
		}

		ServicesEU services = new ServicesEU(list);
		HttpHeaders responseHeaders = new HttpHeaders();

		return new ResponseEntity<ServicesEU>(services, responseHeaders,
				HttpStatus.OK);
	}

	/**
	 * Retrieve the securities control for the sla with id
	 * 
	 * @param ids
	 *            Sla id
	 * @return the securities control
	 */

	@RequestMapping(value = "/securities", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<CapabilitiesEU> update(@RequestBody String ids) {
		CapabilitiesEU capabilitiesEU = null;
		ChoosedCapabilities capabilities;
		ObjectMapper mapper = new ObjectMapper();

		try {
			capabilities = mapper.readValue(ids, ChoosedCapabilities.class);

			capabilitiesEU = builder.getCapabilityEUFromChoosed(capabilities);

		} catch (JsonParseException e) {
			logger.info(e);
		} catch (JsonMappingException e) {
			logger.info(e);
		} catch (IOException e) {
			logger.info(e);
		}

		HttpHeaders responseHeaders = new HttpHeaders();

		return new ResponseEntity<CapabilitiesEU>(capabilitiesEU,
				responseHeaders, HttpStatus.OK);
	}

	/**
	 * Return the SLO for the capabilies and security control choosed
	 * 
	 * @param capabilitiesChoosed
	 *            capabilies and security control choosed
	 * @return The slo
	 */

	@RequestMapping(value = "/agreements", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<SLOStep> agreement(
			@RequestBody String capabilitiesChoosed) {
		CapabilitiesEU capabilitiesEU = null;

		ObjectMapper mapper = new ObjectMapper();
		SLOStep sloStep = null;
		try {
			capabilitiesEU = mapper.readValue(capabilitiesChoosed,
					CapabilitiesEU.class);

			sloStep = builder.getSloStepFromCapabilities(capabilitiesEU);
		} catch (JsonParseException e) {
			logger.info(e);
		} catch (JsonMappingException e) {
			logger.info(e);
		} catch (IOException e) {
			logger.info(e);
		}

		HttpHeaders responseHeaders = new HttpHeaders();

		return new ResponseEntity<SLOStep>(sloStep, responseHeaders,
				HttpStatus.OK);
	}

	/**
	 * Submit the sla and retrieve the offers
	 * 
	 * @param choosed
	 *            security control, capabilities and metrics
	 * @return List of the offers
	 */

	@RequestMapping(value = "/overview", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<OffersInfo> submitSla(@RequestBody String choosed) {

		CapabilitiesEU capabilitiesEU = null;

		ObjectMapper mapper = new ObjectMapper();
		CollectionType offers = new CollectionType();

		List<OfferInfo> offersInfo = new ArrayList<OfferInfo>();
		AgreementOffer overview = null;
		try {
			capabilitiesEU = mapper.readValue(choosed, CapabilitiesEU.class);

			overview = builder.buildOverview(capabilitiesEU,
					capabilitiesEU.getIdsla());
			overview.setName(capabilitiesEU.getIdsla());

			JAXBContext jaxbContext = JAXBContext
					.newInstance(AgreementOffer.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			java.io.StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(overview, sw);

			String dum = sw.toString();

			RestTemplate restPutSla = new RestTemplate();

			String urlToUpdateSla = PropertiesManager.getProperty("sla_manager.endpoint") 
					+ "slas/"
					+ capabilitiesEU.getIdsla();

			HttpHeaders headersPutSla = new HttpHeaders();
			headersPutSla.setContentType(MediaType.TEXT_XML);
			HttpEntity entityPuSla = new HttpEntity(dum, headersPutSla);
			restPutSla.exchange(urlToUpdateSla, HttpMethod.PUT, entityPuSla,
					String.class);

			RestTemplate restSla = new RestTemplate();

			String url = PropertiesManager.getProperty("sla_template.endpoint") 
					+ capabilitiesEU.getIdsla() + "/slaoffers";

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_XML);

			HttpEntity entity = new HttpEntity(dum, headers);

			ResponseEntity<CollectionType> response = restSla.exchange(url,
					HttpMethod.POST, entity, CollectionType.class);
			offers = response.getBody();

		} catch (JsonParseException e) {
			logger.debug(e);
		} catch (JsonMappingException e) {
			logger.debug(e);
		} catch (IOException e) {
			logger.debug(e);
		} catch (JAXBException e) {
			logger.debug(e);
		} catch (Exception e) {
			logger.debug(e);
		}
		HttpHeaders responseHeaders = new HttpHeaders();

		List<ItemList> listItem = offers.getItemList();
		for (int i = 0; i < listItem.size(); i++) {
			OfferInfo info = new OfferInfo(Utility.lastElement(listItem.get(i)
					.getValue(), "/"), Utility.lastElement(listItem.get(i)
							.getValue(), "/"), listItem.get(i).getValue());
			info.setId(Utility.lastElement(listItem.get(i).getValue(), "/"));
			info.setName(Utility.lastElement(listItem.get(i).getValue(), "/"));
			info.setUrl(listItem.get(i).getValue());

			// ! Fix for bad url
			int pos = listItem.get(i).getValue().lastIndexOf("sla-templates");
			String url = listItem.get(i).getValue().substring(0, pos);
			url += listItem.get(i).getValue().substring(pos + 13);
			// End fix

			info.setUrl(url);
			offersInfo.add(info);
		}

		OffersInfo offersToEU = new OffersInfo(overview.getName(), offersInfo);
		return new ResponseEntity<OffersInfo>(offersToEU, responseHeaders,
				HttpStatus.OK);
	}

	/**
	 * Evaluate the offers using the security reasoner component
	 * 
	 * @return List of the evaluated offers
	 */

	@RequestMapping(value = "/evaluate/{slaId}", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<OffersEvaluation> evaluateOffers(@PathVariable(value = "slaId") String slaId, @RequestBody String offers) {
		Type listType = new TypeToken<ArrayList<OfferInfo>>() {}.getType();
		List<OfferInfo> offersInfo = new Gson().fromJson(offers, listType);

		List<OfferEvaluation> evaluations = new ArrayList<OfferEvaluation>();

		for(int i = 0; i < offersInfo.size(); i++){
			RestTemplate restTemplate = new RestTemplate();
			String url = PropertiesManager.getProperty("sla_template.endpoint") + slaId
					+ "/slaoffers/" + offersInfo.get(i).getId();
			try{
				String xml = (String) restTemplate.getForObject(url, String.class);

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_XML);

				HttpEntity entity = new HttpEntity(xml, headers);

				ResponseEntity<String> response = restTemplate.exchange(PropertiesManager.getProperty("security_reasoner.endpoint"),
						HttpMethod.POST, entity, String.class);
				String idSlaCreated = response.getBody();
				
				String urlEvaluate = PropertiesManager.getProperty("security_reasoner.endpoint") + "/" + idSlaCreated
						+ "/score";
				String evaluation = (String) restTemplate.getForObject(urlEvaluate, String.class);
				logger.debug("Offer: "+offersInfo.get(i).getId()+" - Evaluation: "+evaluation);

				evaluations.add(new OfferEvaluation(offersInfo.get(i).getId(), idSlaCreated, evaluation));
			}catch(RestClientException e){
				logger.debug("Rest Client Exception called");
				evaluations.add(new OfferEvaluation(offersInfo.get(i).getId(), "", "error (caiq not found)"));
			}
		}

		OffersEvaluation offersEvaluation = new OffersEvaluation(slaId, evaluations);

		HttpHeaders responseHeaders = new HttpHeaders();
		return new ResponseEntity<OffersEvaluation>(offersEvaluation, responseHeaders,
				HttpStatus.OK);
	}

	@RequestMapping(value = "/rievaluate/{slaId}/{queryType}", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<OffersEvaluation> rievaluateOffers(@PathVariable(value = "slaId") String slaId, @PathVariable(value = "queryType") String queryType, @RequestBody String evaluatedOffers) {
		Type listType = new TypeToken<ArrayList<OfferEvaluation>>() {}.getType();
		List<OfferEvaluation> offerEvaluationInput = new Gson().fromJson(evaluatedOffers, listType);

		List<OfferEvaluation> evaluationsOutput = new ArrayList<OfferEvaluation>();
		String queryString = (queryType == null || queryType.equals("Root")) ? "" : "?category="+queryType;

		for(int i = 0; i < offerEvaluationInput.size(); i++){
			RestTemplate restTemplate = new RestTemplate();

			if(!offerEvaluationInput.get(i).getIdReasoner().equals("")){
				try{

					String urlEvaluate = PropertiesManager.getProperty("security_reasoner.endpoint") + "/" + offerEvaluationInput.get(i).getIdReasoner()
							+ "/score"+queryString;
					String evaluation = (String) restTemplate.getForObject(urlEvaluate, String.class);
					logger.debug("Offer: "+offerEvaluationInput.get(i).getId()+" - Evaluation: "+evaluation);

					evaluationsOutput.add(new OfferEvaluation(offerEvaluationInput.get(i).getId(), offerEvaluationInput.get(i).getIdReasoner(), evaluation));
				}catch(RestClientException e){
					logger.debug("Rest Client Exception called");
					evaluationsOutput.add(new OfferEvaluation(offerEvaluationInput.get(i).getId(), offerEvaluationInput.get(i).getIdReasoner(), "error (category not found)"));
				}
			}else{
				evaluationsOutput.add(offerEvaluationInput.get(i));
			}
		}

		OffersEvaluation offersEvaluationOutput = new OffersEvaluation(slaId, evaluationsOutput);

		HttpHeaders responseHeaders = new HttpHeaders();
		return new ResponseEntity<OffersEvaluation>(offersEvaluationOutput, responseHeaders,
				HttpStatus.OK);
	}

	/**
	 * Retrieve the xml of the offer
	 * 
	 * @param idsla
	 *            id of the sla
	 * @param idoffer
	 *            id of the offer
	 * @return the xml of the offer
	 */

	@RequestMapping(value = "/offerxml/{idsla}/{idoffer}", method = RequestMethod.GET)
	public ResponseEntity<String> getOfferXml(
			@PathVariable(value = "idsla") String idsla,
			@PathVariable(value = "idoffer") String idoffer) {
		RestTemplate restSlo = new RestTemplate();
		String url = PropertiesManager.getProperty("sla_template.endpoint") + idsla
				+ "/slaoffers/" + idoffer;
		String xml = (String) restSlo.getForObject(url, String.class);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/xml; charset=utf-8");

		return new ResponseEntity<String>(xml, responseHeaders, HttpStatus.OK);
	}

	/**
	 * Retrieve the synthetic of the offer
	 * 
	 * @param idsla
	 *            id of the sla
	 * @param idoffer
	 *            id of the offer
	 * @return the synthetic of the offer
	 */

	@RequestMapping(value = "/offersynthetic/{idsla}/{idoffer}", method = RequestMethod.GET)
	public ResponseEntity<SyntheticOffer> getOfferSynthetic(
			@PathVariable(value = "idsla") String idsla,
			@PathVariable(value = "idoffer") String idoffer) {
		RestTemplate restSlo = new RestTemplate();
		String url = PropertiesManager.getProperty("sla_template.endpoint") + idsla
				+ "/slaoffers/" + idoffer;
		String xml = (String) restSlo.getForObject(url, String.class);
		List<Synth> syntList = new ArrayList<Synth>();
		String providerId = "not found";
		String providerName = "not found";
		String providerHardware = "not found";

		try {
			AgreementOffer offer = builder.buildOfferFromXml(xml);
			List<SLOType> sloInfo = builder.getSLOInfo(offer);

			for (int i = 0; i < sloInfo.size(); i++) {
				syntList.add(new Synth(sloInfo.get(i).getMetricREF(), sloInfo
						.get(i).getSLOexpression().getOneOpExpression()
						.getOperator().toString(), sloInfo.get(i)
						.getSLOexpression().getOneOpExpression().getOperand()
						.toString(), sloInfo.get(i).getImportanceWeight()
						.name()));
			}

			List<Term> terms = offer.getTerms().getAll().getAll();
			for (int i = 0; i < terms.size(); i++){
				if(terms.get(i) instanceof ServiceDescriptionTerm){
					ServiceDescriptionTerm descriptionTerm = (ServiceDescriptionTerm) terms.get(i);
					List<ServiceResources> serviceResources = descriptionTerm.getServiceDescription().getServiceResources();
					for (int j = 0; j < serviceResources.size(); j++){
						List<ResourcesProvider> resourcesProvider = serviceResources.get(j).getResourcesProvider();
						for (int z = 0; z < resourcesProvider.size(); z++){
							providerId = resourcesProvider.get(z).getId();
							providerName = resourcesProvider.get(z).getName();
							List<VM> vms = resourcesProvider.get(z).getVM();
							for (int t = 0; t < vms.size(); t++){
								providerHardware = vms.get(t).getHardware();
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e);
		}

		SyntheticOffer syntheticOffer = new SyntheticOffer(syntList, providerId, providerName, providerHardware);
		HttpHeaders responseHeaders = new HttpHeaders();
		return new ResponseEntity<SyntheticOffer>(syntheticOffer, responseHeaders,
				HttpStatus.OK);
	}

	/**
	 * Retrivie the xml of the service
	 * 
	 * @param name
	 *            of the service
	 * @return XML of the service
	 */

	@RequestMapping(value = "/servicexml/{name}", method = RequestMethod.GET)
	public ResponseEntity<String> getServiceXml(
			@PathVariable(value = "name") String name) {

		String xml = builder.getTemplateXML(name);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/xml; charset=utf-8");

		return new ResponseEntity<String>(xml, responseHeaders, HttpStatus.OK);

	}

	/**
	 * Submit the offer selected
	 * 
	 * @param offerSelected
	 *            (json with idsla and xml of the offer)
	 * @return the xml of the offer
	 */

	@RequestMapping(value = "/submitOffer", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<String> submitOffer(@Context HttpServletRequest request, @RequestBody String offerSelected) {
		ObjectMapper mapper = new ObjectMapper();
		String xml = null;
		try {
			OfferChoosed offer = mapper.readValue(offerSelected,
					OfferChoosed.class);
			RestTemplate restSla = new RestTemplate();
			String url = PropertiesManager.getProperty("sla_template.endpoint")
					+ "{id}/slaoffers/current";
			String id = offer.getIdsla();

			if (offer.getXml() == null || offer.getXml().length() == 0) {
				RestTemplate restSlo = new RestTemplate();
				String urlXML = PropertiesManager.getProperty("sla_template.endpoint") + offer.getIdsla()
				+ "/slaoffers/" + offer.getId();
				String xmlResponse = (String) restSlo.getForObject(urlXML, String.class);
				offer.setXml(xmlResponse);
			}

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_XML);
			HttpEntity entity = new HttpEntity(offer.getXml(), headers);
			ResponseEntity<String> response = restSla.exchange(url,
					HttpMethod.PUT, entity, String.class, id);

			xml = response.getBody();
		} catch (IOException e) {
			logger.info(e);
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/xml; charset=utf-8");

		return new ResponseEntity<String>(xml, responseHeaders, HttpStatus.OK);
	}

	/**
	 * Retrieve the list of the sla in state
	 * 
	 * @param state
	 *            <negotiating or incomplete>
	 * @return A list of id
	 */

	@RequestMapping(value = "/incomplete/{state}", method = RequestMethod.GET)
	public ResponseEntity<SlaIds> getNegotiating(
			@PathVariable(value = "state") String state) {
		String url = PropertiesManager.getProperty("sla_manager_state.endpoint")  + "?state="
				+ state;
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/xml; charset=utf-8");

		RestTemplate rest = new RestTemplate();

		CollectionTypeTemp collection = (CollectionTypeTemp) rest.getForObject(
				url, CollectionTypeTemp.class);

		List<Item> itemList = collection.getItem();
		List<IdList> idList = new ArrayList<IdList>();

		for (int i = 0; i < itemList.size(); i++) {
			IdList id = new IdList();
			id.setId(Utility.lastElement(itemList.get(i).getValue(), "/"));
			idList.add(id);
		}

		SlaIds slas = new SlaIds(idList);
		return new ResponseEntity<SlaIds>(slas, responseHeaders, HttpStatus.OK);
	}

	/**
	 * Sign the sla with id
	 * 
	 * @param id
	 *            of sla
	 */

	@RequestMapping(value = "/sign/{id}", method = RequestMethod.POST)
	public void sign(@PathVariable(value = "id") String id) {
		String url = PropertiesManager.getProperty("sla_manager_state.endpoint") + "/" + id
				+ "/userSign";
		RestTemplate restSla = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.TEXT_XML);

		HttpEntity entity = new HttpEntity(null, headers);

		restSla.exchange(url, HttpMethod.POST, entity, String.class);
	}

	/**
	 * Implement the sla with id
	 * 
	 * @param id
	 *            of sla
	 * @return Implementation info
	 */

	@RequestMapping(value = "/implement/{id}", method = RequestMethod.POST)
	public ResponseEntity<Implement> implement(
			@PathVariable(value = "id") String id) {

		String url = PropertiesManager.getProperty("planning.endpoint"); 

		RestTemplate restPlan = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		String data = "{\"sla_id\":\"" + id + "\"}";
		HttpEntity<String> entity = new HttpEntity<String>(data, headers);
		Implement implement = null;
		try{
			ResponseEntity<PlanningActivity> response = restPlan.exchange(url,
					HttpMethod.POST, entity, PlanningActivity.class);

			implement = new Implement(response.getBody().getId(),
					response.getBody().getSlaId(), response.getBody().getState()
					.toString(), response.getBody().getActivePlanId());

		}catch(RestClientException e){
			logger.debug(e);
			logger.debug("Implementation exception");
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/xml; charset=utf-8");

		if(implement != null){

			try{
				String url2 = PropertiesManager.getProperty("sla_manager_state.endpoint") + "/" + id
						+ "/implement";
				RestTemplate restSla = new RestTemplate();
				HttpHeaders headers2 = new HttpHeaders();
				headers2.setContentType(MediaType.TEXT_XML);

				HttpEntity entity2 = new HttpEntity(null, headers2);

				restSla.exchange(url2, HttpMethod.POST, entity2, String.class);
			}catch(RestClientException e){
				logger.debug(e);
				logger.debug("Sla update exception");
			}
		}

		if(implement != null){
			return new ResponseEntity<Implement>(implement, responseHeaders,
					HttpStatus.OK);
		}else{
			return new ResponseEntity<Implement>(null, responseHeaders,
					HttpStatus.NOT_IMPLEMENTED);
		}
	}

	/**
	 * Retrieve a list of implementation info
	 * 
	 * @return list of implementation info
	 */

	@RequestMapping(value = "/monitoring", method = RequestMethod.GET)
	public ResponseEntity<PlanImplementations> monitoring() {

		List<Implement> plans = new ArrayList<Implement>();
		String url = PropertiesManager.getProperty("planning.endpoint"); 
		RestTemplate restplan = new RestTemplate();

		try {

			CollectionTypeJson collection = (CollectionTypeJson) restplan.getForObject(
					url, CollectionTypeJson.class);

			List<ItemJson> itemList = collection.getItemList();


			for (int i = 0; i < itemList.size(); i++) {
				ItemJson item = itemList.get(i);

				RestTemplate restPlane = new RestTemplate();

				try {
					PlanningActivity plane = (PlanningActivity) restPlane.getForObject(
							item.getItem(), PlanningActivity.class);
					if (plane != null) {	
						Implement implement = new Implement(plane.getId(),
								plane.getSlaId(), plane.getState().toString(),
								plane.getActivePlanId());
						plans.add(implement);
					}
				}catch (HttpStatusCodeException e) {
					plans.add(null);
					logger.debug("Plan get exception");
				} catch (Exception e) {
					logger.debug(e);
				}
			}

			for (int i = 0; i < plans.size(); i++) {
				if(plans.get(i) != null){
					try {
						RestTemplate restSla = new RestTemplate();
						String urlSla = PropertiesManager.getProperty("sla_manager_state.endpoint") + "/" + plans.get(i).getSlaid() + "/status";
						String status = (String) restSla.getForObject(
								urlSla, String.class);
						plans.get(i).setSlastate(status);
					}catch (HttpStatusCodeException e) {
						logger.debug("Get Sla status for id: "+plans.get(i).getSlaid()+" get exception");
						plans.get(i).setSlastate("UNKNOWN");
					} catch (Exception e) {
						logger.debug(e);
					}
				}else{
					plans.get(i).setSlastate("UNKNOWN");
				}
			}

		} catch (HttpStatusCodeException e) {
			logger.debug("Status Code", e);
		} catch (Exception e) {
			logger.debug(e);
		}

		PlanImplementations implementatios = new PlanImplementations(plans);
		HttpHeaders responseHeaders = new HttpHeaders();
		return new ResponseEntity<PlanImplementations>(implementatios,
				responseHeaders, HttpStatus.OK);

	}

	/**
	 * Retrieve the metrics value for plan for the plan wit idplan and idsla
	 * 
	 * @param idplan
	 *            is the plan id
	 * @param idsla
	 *            is the id sla
	 * @return Monitoring details
	 */

	@RequestMapping(value = "/metricsDetails/{idplan}/{idsla}", method = RequestMethod.GET)
	public ResponseEntity<MonitorDetails> getMetricValues(
			@PathVariable(value = "idplan") String idplan,
			@PathVariable(value = "idsla") String idsla) {
		List<Metricsdetail> metricsList = new ArrayList<Metricsdetail>();

		List<String> componentsName = new ArrayList<String>();
		List<String> metrics = new ArrayList<String>();
		Map<String, Metricsdetail> metricsMap = new HashMap<String, Metricsdetail>();

		try {

			RestTemplate restPlane = new RestTemplate();
			String url = PropertiesManager.getProperty("planning.endpoint") + "/" + idplan;

			PlanningActivity plane = (PlanningActivity) restPlane.getForObject(
					url, PlanningActivity.class);

			if (plane != null) {

				List<Allocation> allocations = plane.getSupplyChain()
						.getAllocations();

				for (int i = 0; i < allocations.size(); i++) {
					List<String> components = allocations.get(i)
							.getComponentIds();
					for (int j = 0; j < components.size(); j++) {
						componentsName.add(components.get(j));

					}
				}
			}

			String urlSla = PropertiesManager.getProperty("sla_manager_state.endpoint") + "/"
					+ idsla;
			RestTemplate restSla = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();

			String xml = (String) restSla.getForObject(urlSla, String.class);

			AgreementOffer offer = builder.buildOfferFromXml(xml);
			List<SLOType> sloInfo = builder.getSLOInfo(offer);

			String sort = "{\"timestamp\":-1}";
			metrics = builder.getMetricRef(offer);
			Map<String, String> metricsNameMap = builder
					.getMetricNameMapping(offer);

			for (int i = 0; i < componentsName.size(); i++) {

				for (int j = 0; j < metrics.size(); j++) {

					String metrica = metrics.get(j);
					String metricaFilter = metrica;
					if(metrica.equals("level_of_diversity_m2")){
						metricaFilter = "diversity_level_wp_msr2";
					}else if(metrica.equals("level_of_redundancy_m1")){
						metricaFilter = "number_of_servers_wp_msr1";
					}

					String filter = "{\"$and\":[{\"labels\":{\"$in\":[\"sla_id_"
							+ idsla
							+ "\"]}},"
							+ "{\"labels\":{\"$in\":[\"security_metric_"
							+ metricaFilter + "\"]}}]}";

					HttpHeaders headersMON = new HttpHeaders();
					headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

					UriComponentsBuilder uriBuilder = UriComponentsBuilder
							.fromHttpUrl(PropertiesManager.getProperty("monitor.endpoint"))
							.queryParam("filter", filter)
							.queryParam("sort", sort);

					logger.info("monitoring filter query: " + filter);

					try {
						HttpEntity<?> entity = new HttpEntity<>(headersMON);
						RestTemplate restTemplate = new RestTemplate();

						HttpEntity<String> response = restTemplate.exchange(
								uriBuilder.build().encode().toUri(),
								HttpMethod.GET, entity, String.class);

						String json = response.getBody();
						JSONArray jsonArray;
						jsonArray = new JSONArray(json);

						for (int ii = 0; ii < 1; ii++) {

							JSONObject result = jsonArray.getJSONObject(ii);

							long timestamp = result.getLong("timestamp");
							String datametric = result.getString("data");

							Timestamp stamp = new Timestamp(timestamp*1000);
							Date date = new Date(stamp.getTime());
							String longDate = new SimpleDateFormat(
									"dd.MM.yyyy - HH:mm:ss.S").format(date);

							Metricsdetail currentMetric = new Metricsdetail("",
									metricsNameMap.get(metrica), "", "",
									datametric, longDate, "");
							metricsList.add(currentMetric);

						}
					} catch (HttpStatusCodeException e) {
						logger.debug("Status Code", e);
					} catch (JSONException e) {
						logger.debug(e);
					} catch (Exception e) {
						logger.debug(e);
					}
				}

			}
			try {

				for (int i = 0; i < sloInfo.size(); i++) {
					String sloOperator = sloInfo.get(i).getSLOexpression()
							.getOneOpExpression().getOperator().toString();

					String sloOperand = sloInfo.get(i).getSLOexpression()
							.getOneOpExpression().getOperand()
							.toString();

					for (int j = 0; j < metricsList.size(); j++) {

						if (metricsList.get(j).getMetricname()
								.equals(sloInfo.get(i).getMetricREF())) {
							metricsList.get(j).setSloOperator(sloOperator);
							metricsList.get(j).setSloOperand(sloOperand);
						}
					}
				}
			} catch (Exception e) {
				logger.info(e);
			}

			//Check metric state
			for (int i = 0; i < metricsList.size(); i++) {
				if(metricsList.get(i).getMeasuredvalue().equals("----------------")){
					metricsList.get(i).setState("UNAVAILABLE");
				}else{
					if(metricsList.get(i).getSloOperator().equals("EQUAL")){
						if(metricsList.get(i).getSloOperand().equals(metricsList.get(i).getMeasuredvalue())){
							metricsList.get(i).setState("VALID");
						}else{
							metricsList.get(i).setState("NOT VALID");
						}
					}else if(metricsList.get(i).getSloOperator().equals("NOT_EQUAL")){
						if(!metricsList.get(i).getSloOperand().equals(metricsList.get(i).getMeasuredvalue())){
							metricsList.get(i).setState("VALID");
						}else{
							metricsList.get(i).setState("NOT VALID");
						}
					}else if(metricsList.get(i).getSloOperator().equals("GREATER_THAN")){
						try{
							int operand = Integer.valueOf(metricsList.get(i).getSloOperand());
							int measuredValue = Integer.valueOf(metricsList.get(i).getMeasuredvalue());
							if(measuredValue > operand){
								metricsList.get(i).setState("VALID");
							}else{
								metricsList.get(i).setState("NOT VALID");
							}
						}catch(NumberFormatException e){
							metricsList.get(i).setState("NOT VALID");
						}
					}else if(metricsList.get(i).getSloOperator().equals("GREATER_THAN_OR_EQUAL")){
						try{
							int operand = Integer.valueOf(metricsList.get(i).getSloOperand());
							int measuredValue = Integer.valueOf(metricsList.get(i).getMeasuredvalue());
							if(measuredValue >= operand){
								metricsList.get(i).setState("VALID");
							}else{
								metricsList.get(i).setState("NOT VALID");
							}
						}catch(NumberFormatException e){
							metricsList.get(i).setState("NOT VALID");
						}
					}else if(metricsList.get(i).getSloOperator().equals("LESS_THAN")){
						try{
							int operand = Integer.valueOf(metricsList.get(i).getSloOperand());
							int measuredValue = Integer.valueOf(metricsList.get(i).getMeasuredvalue());
							if(measuredValue < operand){
								metricsList.get(i).setState("VALID");
							}else{
								metricsList.get(i).setState("NOT VALID");
							}
						}catch(NumberFormatException e){
							metricsList.get(i).setState("NOT VALID");
						}
					}else if(metricsList.get(i).getSloOperator().equals("LESS_THAN_OR_EQUAL")){
						try{
							int operand = Integer.valueOf(metricsList.get(i).getSloOperand());
							int measuredValue = Integer.valueOf(metricsList.get(i).getMeasuredvalue());
							if(measuredValue <= operand){
								metricsList.get(i).setState("VALID");
							}else{
								metricsList.get(i).setState("NOT VALID");
							}
						}catch(NumberFormatException e){
							metricsList.get(i).setState("NOT VALID");
						}
					}
				}

			}

			for (int i = 0; i < metricsList.size(); i++) {
				metricsMap.put(metricsList.get(i).getMetricname(),
						metricsList.get(i));
			}

			builder.addInfoForMonitoring(offer, metricsMap);

		} catch (HttpStatusCodeException e) {
			logger.debug("Status Code", e);
		} catch (Exception e) {
			logger.debug(e);
		}

		MonitorDetails monitorDetails = new MonitorDetails(new ArrayList(
				metricsMap.values()));
		HttpHeaders responseHeaders = new HttpHeaders();

		return new ResponseEntity<MonitorDetails>(monitorDetails,
				responseHeaders, HttpStatus.OK);

	}

	/**
	 * Retrieve the url for the security reasoner and the plan id
	 * 
	 * @param idplan
	 *            is the implementation plan id
	 * @return A json within url for the security reasoner ad the plan id
	 */

	@RequestMapping(value = "/implemented/{idplan}", method = RequestMethod.GET)
	public String getImplementedPlan(
			@PathVariable(value = "idplan") String idplan) {

		String urlManage = "";
		try {
			String url = PropertiesManager.getProperty("implementation.endpoint") + "/" + idplan;

			ImplementationPlan plan = (ImplementationPlan) rest.getForObject(url, ImplementationPlan.class);

			List<Pool> pools = plan.getPools();

			for (int i = 0; i < pools.size(); i++) {
				List<Vm> vms = pools.get(i).getVms();

				for (int j = 0; j < vms.size(); j++) {
					for(int z = 0; z < vms.get(j).getComponents().size(); z++){
						if(("haproxy").equals(vms.get(j).getComponents().get(z).getRecipe())){
							urlManage =  "http://"+vms.get(j).getPublicIp()+PropertiesManager.getProperty("manage.endpoint");
							break;
						}
					} 
				}
			}
		}catch(Exception e){
			logger.error(e.getMessage());
		}

		String data = "{\"url\":\"" + urlManage + "\",\"plan\":" + "" + "}";

		try {
			data = "{\"url\":\"" + urlManage + "\",\"plan\":\"" + idplan
					+ "\"}";
			return data;
		} catch (Exception e) {
			logger.info(e);
		}
		return data;
	}

	/**
	 * Retrieve the xml of the sla
	 * 
	 * @param id
	 *            sla id
	 * @return The xml of the sla
	 */

	@RequestMapping(value = "/slaxml/{id}", method = RequestMethod.GET)
	public ResponseEntity<String> getSlaXml(
			@PathVariable(value = "id") String id) {

		RestTemplate restSla = new RestTemplate();
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/xml; charset=utf-8");
		String xml = (String) restSla.getForObject(PropertiesManager.getProperty("cloud_sla.endpoint")
				+ id,
				String.class);

		return new ResponseEntity<String>(xml, responseHeaders, HttpStatus.OK);

	}

	/**
	 * Retrieve node info for the plan with id
	 * 
	 * @param id
	 *            the plan id
	 * @return A list of node info
	 */

	@RequestMapping(value = "/nodes/{id}", method = RequestMethod.GET)
	public ResponseEntity<NodesInfo> getNodes(
			@PathVariable(value = "id") String id) {

		RestTemplate restPlane = new RestTemplate();
		List<Node> nodeList = new ArrayList<Node>();

		try {
			String url = PropertiesManager.getProperty("implementation.endpoint") + "/" + id;

			ImplementationPlan plan = (ImplementationPlan) restPlane
					.getForObject(url, ImplementationPlan.class);

			List<Pool> pools = plan.getPools();

			for (int i = 0; i < pools.size(); i++) {
				List<Vm> vms = pools.get(i).getVms();

				for (int j = 0; j < vms.size(); j++) {
					vms.get(j).getPublicIp();
					List<String> privates = new ArrayList<String>();
					List<String> mechanisms = new ArrayList<String>();
					for (int k = 0; k < vms.get(j).getComponents().size(); k++) {
						privates.addAll(vms.get(j).getComponents().get(k)
								.getPrivateIps());
						mechanisms
						.add(vms.get(j).getComponents().get(k)
								.getCookbook()
								+ ":"
								+ vms.get(j).getComponents().get(k)
								.getId());

					}
					Node node = new Node(vms.get(j).getPublicIp(), vms.get(j)
							.getPublicIp(), privates, mechanisms);
					nodeList.add(node);
				}

			}

		} catch (Exception e) {
			logger.info(e);
		}
		NodesInfo nodes = new NodesInfo(nodeList);
		HttpHeaders responseHeaders = new HttpHeaders();
		return new ResponseEntity<NodesInfo>(nodes, responseHeaders,
				HttpStatus.OK);

	}

	/**
	 * Retrieve the info for the synthetic view
	 * 
	 * @param id
	 *            of the plan
	 * @return A list of slo info
	 */

	@RequestMapping(value = "/synt/{id}", method = RequestMethod.GET)
	public ResponseEntity<Synthetic> getSynt(
			@PathVariable(value = "id") String id) {

		List<Synth> syntList = new ArrayList<Synth>();
		try {
			RestTemplate restPlane = new RestTemplate();
			String url = PropertiesManager.getProperty("implementation.endpoint") + "/" + id;

			ImplementationPlan plan = (ImplementationPlan) restPlane
					.getForObject(url, ImplementationPlan.class);

			for (int i = 0; i < plan.getSlos().size(); i++) {
				syntList.add(new Synth(plan.getSlos().get(i).getMetricId(),
						plan.getSlos().get(i).getOperator(), plan.getSlos()
						.get(i).getValue(), plan.getSlos().get(i)
						.getImportanceLevel().name()));

			}

		} catch (Exception e) {
			logger.info(e);
		}
		Synthetic synthetic = new Synthetic(syntList, id);
		HttpHeaders responseHeaders = new HttpHeaders();
		return new ResponseEntity<Synthetic>(synthetic, responseHeaders,
				HttpStatus.OK);

	}

	@RequestMapping(value = "/syntsla/{id}", method = RequestMethod.GET)
	public ResponseEntity<Synthetic> getSyntFromSla(
			@PathVariable(value = "id") String id) {

		List<Synth> syntList = new ArrayList<Synth>();
		builder.setCloudSlaEndpoin(PropertiesManager.getProperty("cloud_sla.endpoint"));
		try {
			AgreementOffer offer = builder.getTemplateObjectFromId(id);
			List<SLOType> sloInfo = builder.getSLOInfo(offer);

			for (int i = 0; i < sloInfo.size(); i++) {
				syntList.add(new Synth(sloInfo.get(i).getMetricREF(), sloInfo
						.get(i).getSLOexpression().getOneOpExpression()
						.getOperator().toString(), sloInfo.get(i)
						.getSLOexpression().getOneOpExpression().getOperand()
						.toString(), sloInfo.get(i).getImportanceWeight()
						.name()));
			}

		} catch (Exception e) {
			logger.info(e);
		}

		Synthetic synthetic = new Synthetic(syntList, id);
		HttpHeaders responseHeaders = new HttpHeaders();
		return new ResponseEntity<Synthetic>(synthetic, responseHeaders,
				HttpStatus.OK);

	}
	
	@RequestMapping(value = "/renegotiate/{id}", method = RequestMethod.POST)
	public ResponseEntity<String> renegotiateSla(
			@PathVariable(value = "id") String id, @RequestBody String newSynthetic) {
		logger.info("Renegotiate called");

		builder.setCloudSlaEndpoin(PropertiesManager.getProperty("cloud_sla.endpoint"));
		try {
			AgreementOffer offer = builder.getTemplateObjectFromId(id);
			List<SLOType> sloInfo = builder.getSLOInfo(offer);
			
			Type listType = new TypeToken<ArrayList<Synth>>() {}.getType();
			List<Synth> synts = new Gson().fromJson(newSynthetic, listType);
			
			for (int i = 0; i < synts.size(); i++) {
				Synth synth = synts.get(i);
				sloInfo.get(i).getSLOexpression().getOneOpExpression().setOperand(synth.value);	
			}
			builder.getSLOInfo(offer);
			logger.info("Sla edited");

			String editedSla = builder.buildXmlFromOffer(offer);
			logger.info("Sla Offer xml converted.");
			
			String url = PropertiesManager.getProperty("sla_manager_state.endpoint") + "/" + id + "/reNegotiate";

			
			RestTemplate restSla = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_XML);

			HttpEntity entity = new HttpEntity(null, headers);

			restSla.exchange(url, HttpMethod.POST, entity, String.class);
			logger.info("Sla state edited: RENEGOTIATE");
			
			url = PropertiesManager.getProperty("cloud_sla.endpoint") + id;
			HttpEntity entityPost = new HttpEntity(editedSla, headers);

			restSla.exchange(url, HttpMethod.PUT, entityPost, String.class);
			logger.info("New Sla put into Sla Manager Ok");
			
			url = PropertiesManager.getProperty("terminate.endpoint"); 
			String data = "{\"label\":\"UPDATE\",\"sla_id\":\"" + id + "\"}";
			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<String> entityRenegotiate = new HttpEntity<String>(data, headers);

			restSla.exchange(url, HttpMethod.POST, entityRenegotiate, String.class);
			logger.info("Renegotiate started correctly!");

			HttpHeaders responseHeaders = new HttpHeaders();
			return new ResponseEntity<String>("{\"response:\":\"The renegotiation is started correctly!\"}", responseHeaders,
					HttpStatus.OK);
		} catch (Exception e) {
			logger.info(e);
			HttpHeaders responseHeaders = new HttpHeaders();
			return new ResponseEntity<String>("{\"response:\":\"An error is occurred during the start of renegotiation!\"}", responseHeaders,
					HttpStatus.NOT_IMPLEMENTED);
		}



	}

	@RequestMapping(value = "/terminate/{id}", method = RequestMethod.POST)
	public int terminate(@PathVariable(value = "id") String id) {
		String url = PropertiesManager.getProperty("terminate.endpoint"); 
		String data = "{\"label\":\"TERMINATE\",\"sla_id\":\"" + id + "\"}";
		int status = -1;
		try {
			RestTemplate restTerminate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<String> entity = new HttpEntity<String>(data, headers);

			ResponseEntity<String> response = restTerminate.exchange(url,
					HttpMethod.POST, entity, String.class);
			status = response.getStatusCode().value();
		} catch (HttpStatusCodeException e) {
			logger.debug("Status Code", e);
			status = e.getStatusCode().value();
		} catch (Exception e) {
			logger.info(e);
		}
		return status;
	}

	@RequestMapping(value = "/detailendpoint", method = RequestMethod.GET) 
	public ResponseEntity<String> detailendpoint() {
		HttpHeaders responseHeaders = new HttpHeaders();

		return new ResponseEntity<String>(
				"{\"url\":\""+PropertiesManager.getProperty("negotiation_metric_detail.endpoint")+"\"}", responseHeaders,
				HttpStatus.OK);
	}

	@RequestMapping(value = "/annotation/{id}", method = RequestMethod.GET) 
	public ResponseEntity<AnnotationsJson> getAnnotation(@PathVariable(value = "id") String id) {
		HttpHeaders responseHeaders = new HttpHeaders();
		Annotations ann = new Annotations();
		String url = PropertiesManager.getProperty("sla_manager.endpoint") + "/slas/" + id + "/annotations";
		try {
			RestTemplate restAnnotation = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_XML);

			HttpEntity entity = new HttpEntity("", headers);

			ResponseEntity<Annotations> response = restAnnotation.exchange(url,
					HttpMethod.GET, entity, Annotations.class);
			ann = response.getBody();
		} catch (HttpStatusCodeException e) {
			logger.debug("Status Code", e);

		} catch (Exception e) {
			logger.info(e);
		}

		List<AnnotationJson> annotationJsonList = new ArrayList<AnnotationJson>();
		for (int i = 0; i < ann.getAnnotation().size(); i++) {
			AnnotationJson annotationJson= new AnnotationJson();
			annotationJson.setDescr(String.valueOf(ann.getAnnotation().get(i).getDescr()));
			annotationJson.setId(ann.getAnnotation().get(i).getId());
			String timeStamp = new SimpleDateFormat("dd.MM.yyyy - HH.mm.ss.S").format(ann.getAnnotation().get(i).getTimestamp());
			annotationJson.setData(timeStamp);
			annotationJsonList.add(annotationJson);
		}

		
		AnnotationsJson annotationsJson = new AnnotationsJson(id, annotationJsonList);

		return new ResponseEntity<AnnotationsJson>(annotationsJson, responseHeaders,
				HttpStatus.OK);
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET) 
	@ResponseBody
	public String getTest() {
		return "test";
	}

	protected static String getBaseEnvLinkURL() {

		String baseEnvLinkURL = null;
		HttpServletRequest currentRequest = ((ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes()).getRequest();

		baseEnvLinkURL = "http://" + currentRequest.getLocalName();
		if (currentRequest.getLocalPort() != 80) {
			baseEnvLinkURL += ":" + currentRequest.getLocalPort();
		}
		if (!StringUtils.isEmpty(currentRequest.getContextPath())) {
			baseEnvLinkURL += currentRequest.getContextPath();
		}
		return baseEnvLinkURL;
	}
}
