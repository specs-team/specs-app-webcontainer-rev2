package specs.sws_application.utility;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import specs.sw_application.frontend.entities.CapabilitiesEU;
import specs.sw_application.frontend.entities.CapabilityEU;
import specs.sw_application.frontend.entities.CapabilitySLOStep;
import specs.sw_application.frontend.entities.ChoosedCapabilities;
import specs.sw_application.frontend.entities.FrameworkEU;
import specs.sw_application.frontend.entities.FrameworkSLO;
import specs.sw_application.frontend.entities.IdList;
import specs.sw_application.frontend.entities.MetricEU;
import specs.sw_application.frontend.entities.MetricSLO;
import specs.sw_application.frontend.entities.SLOStep;
import specs.sw_application.frontend.entities.SecurityControlEU;
import specs.sw_application.frontend.entities.MonitorDetails.Metricsdetail;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.slo.ServiceProperties;
import eu.specs.datamodel.agreement.slo.Variable;
import eu.specs.datamodel.agreement.terms.GuaranteeTerm;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.agreement.terms.Terms.All;
import eu.specs.datamodel.control_frameworks.AbstractSecurityControl;
import eu.specs.datamodel.control_frameworks.ccm.CCMSecurityControl;
import eu.specs.datamodel.control_frameworks.nist.NISTSecurityControl;
import eu.specs.datamodel.metrics.AbstractMetricType;
import eu.specs.datamodel.sla.sdt.CapabilityType;
import eu.specs.datamodel.sla.sdt.ControlFramework;
import eu.specs.datamodel.sla.sdt.ObjectiveList;
import eu.specs.datamodel.sla.sdt.SLOType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType;
import eu.specs.datamodel.sla.sdt.WeightType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType.Capabilities;

public class BuilderExtractor {

	ConcurrentHashMap<String, String> servicesUrl = new ConcurrentHashMap<String, String>();
	ConcurrentHashMap<String, AgreementOffer> servicesTemplate = new ConcurrentHashMap<String, AgreementOffer>();
	ConcurrentHashMap<String, String> servicesXml = new ConcurrentHashMap<String, String>();
	String cloudSlaEndpoint = "";
	private static final Logger logger = LogManager
			.getLogger(BuilderExtractor.class);


	/**
	 * Set the endpoint for cloud sla
	 * 
	 * @param cloudSlaEndpoint
	 *            url of endpoint
	 */

	public void setCloudSlaEndpoin(String cloudSlaEndpoint) {
		this.cloudSlaEndpoint = cloudSlaEndpoint;
	}

	/**
	 * Store the url of the service in a map
	 * 
	 * @param name
	 *            of the service
	 * @param url
	 *            of the service
	 */

	public void putServiceUrl(String name, String url) {
		servicesUrl.put(name, url);
	}

	/**
	 * Provide the AgreementOffer for the service
	 * @param service is the service's name
	 * @return The AgreenOffer object
	 */

	public AgreementOffer getTemplateFromService(String service, HttpServletRequest request) {
		AgreementOffer offer = null;
		offer = getTemplateObject(service, request);
		if (!servicesTemplate.containsKey(service)) {
			servicesTemplate.put(service, offer);
		}
		return offer;
	}

	/**
	 * Starting from service, capabilities, security control selected create the final sla
	 * @param capabilitiesEU within the capabilities, security control selected from the user
	 * @param service is the service's name
	 * @return
	 */

	public AgreementOffer buildOverview(CapabilitiesEU capabilitiesEU,
			String service) {
		AgreementOffer offer = getTemplateObjectFromId(service);
		All all = offer.getTerms().getAll();
		List<Term> terms = all.getAll();
		List<String> caps = getCapabilityId(capabilitiesEU);
		List<String> metricRefs = getMetriRef(capabilitiesEU);
		Map<String, String> metricNameRef = getMetricNameRef(offer);

		for (int i = terms.size() - 1; i >= 0; i--) {
			if (terms.get(i) instanceof GuaranteeTerm) {
				GuaranteeTerm guarantee = (GuaranteeTerm) terms.get(i);
				if (!caps.contains(Utility.getFromQuotes(guarantee.getName()))) {
					terms.remove(i);
				} else {
					ObjectiveList objectiveList = guarantee
							.getServiceLevelObjective().getCustomServiceLevel()
							.getObjectiveList();
					List<SLOType> slos = objectiveList.getSLO();
					int j = 0;
					while (j < slos.size()) {
						if (!metricRefs.contains(metricNameRef.get(slos.get(j)
								.getMetricREF()))) {
							slos.remove(j);
						} else {

							MetricSLO metricSLO = getMetricValue(
									capabilitiesEU,
									Utility.getFromQuotes(guarantee.getName()),
									metricNameRef.get(slos.get(j)
											.getMetricREF()));
							if (metricSLO != null) {
								slos.get(j).setImportanceWeight(
										WeightType.valueOf(metricSLO
												.getImportance()));
								slos.get(j).getSLOexpression()
								.getOneOpExpression()
								.setOperand(metricSLO.getOperand());
								slos.get(j)
								.getSLOexpression()
								.getOneOpExpression()
								.setOperator(
										Utility.convertTOSLOOperator(metricSLO
												.getOperator()));
							}
							j++;
						}
					}
				}
			} else if (terms.get(i) instanceof ServiceDescriptionTerm) {
				ServiceDescriptionTerm serviceDescTerm = (ServiceDescriptionTerm) terms
						.get(i);
				ServiceDescriptionType servDesc = serviceDescTerm
						.getServiceDescription();
				Capabilities capabilities = servDesc.getCapabilities();

				List<CapabilityType> capabilityList = capabilities
						.getCapability();

				servDesc.getSecurityMetrics().getSecurityMetric().get(0)
				.getReferenceId();

				boolean found = false;
				int j = 0;
				while (j < capabilityList.size()) {
					found = false;
					for (int k = 0; k < capabilitiesEU.getCapabilities().size(); k++) {
						if (capabilityList
								.get(j)
								.getId()
								.equals(capabilitiesEU.getCapabilities().get(k)
										.getId())) {
							found = true;
							ControlFramework controlFramework = capabilityList
									.get(j).getControlFramework();
							updateFramework(controlFramework, capabilitiesEU
									.getCapabilities().get(k).getFrameworks()
									.get(0));
							break;
						}
					}
					if (!found) {
						capabilityList.remove(j);
						// remove from <wsag:ServiceProperties
						// wsag:Name="//specs:capability[@id='WEBPOOL']"
					} else {
						j++;
					}
				}
				int k = 0;
				while (k < servDesc.getSecurityMetrics().getSecurityMetric()
						.size()) {
					if (!metricRefs.contains(servDesc.getSecurityMetrics()
							.getSecurityMetric().get(k).getReferenceId())) {
						servDesc.getSecurityMetrics().getSecurityMetric()
						.remove(k);
					} else {
						k++;
					}
				}

			} else if (terms.get(i) instanceof ServiceProperties) {
				ServiceProperties properties = (ServiceProperties) terms.get(i);
				if (!caps.contains(Utility.getFromQuotes(properties.getName()))) {
					terms.remove(i);
				} else {
					properties.getVariableSet().getVariables();

					int j = 0;
					while (j < properties.getVariableSet().getVariables()
							.size()) {
						if (!metricRefs.contains(properties.getVariableSet()
								.getVariables().get(j).getMetric())) {
							properties.getVariableSet().getVariables()
							.remove(j);
						} else {
							j++;
						}
					}
				}
			}
		}
		return offer;
	}

	/**
	 * Return the SLO of the metric id
	 * @param capabilitiesEU All cpability info
	 * @param capabilityId is the capability's id
	 * @param metricId
	 * @return The slo for metricid
	 */

	public MetricSLO getMetricValue(CapabilitiesEU capabilitiesEU,
			String capabilityId, String metricId) {
		List<CapabilityEU> capabilities = capabilitiesEU.getCapabilities();
		for (int i = 0; i < capabilities.size(); i++) {
			if (capabilities.get(i).getId().equals(capabilityId)) {
				List<SecurityControlEU> controls = capabilities.get(i)
						.getFrameworks().get(0).getSecurityControls();
				for (int j = 0; j < controls.size(); j++) {
					for (int k = 0; k < controls.get(j).getMetrics().size(); k++) {
						if (controls.get(j).getMetrics().get(k).getId()
								.equals(metricId))
							return controls.get(j).getMetrics().get(k);
					}
				}
			}
		}

		return null;
	}

	/**
	 * Update the importance of the security control
	 * @param control is the Control Framework
	 * @param fameworkEU within the securities control to update
	 */

	public void updateFramework(ControlFramework control, FrameworkEU fameworkEU) {
		List<AbstractSecurityControl> securities = control.getSecurityControl();
		List<SecurityControlEU> controls = fameworkEU.getSecurityControls();
		int i = 0;
		while (i < securities.size()) {
			boolean found = false;
			for (int j = 0; j < controls.size() && found == false; j++) {
				if (securities.get(i) instanceof NISTSecurityControl) {
					NISTSecurityControl nist = (NISTSecurityControl) securities
							.get(i);
					if (controls.get(j).getId().equals(nist.getId())) {
						found = true;
						((NISTSecurityControl) securities.get(i))
						.setImportanceWeight(WeightType
								.fromValue(controls.get(j).getWeight()));
					}
				} else {
					CCMSecurityControl ccm = (CCMSecurityControl) securities
							.get(i);
					if (controls.get(j).getId().equals(ccm.getId())) {
						found = true;
						((CCMSecurityControl) securities.get(i))
						.setImportanceWeight(WeightType
								.fromValue(controls.get(j).getWeight()));
					}
				}
			}
			if (!found)
				securities.remove(i);
			else
				i++;
		}
	}

	/**
	 * Provide the metricref for the capabilities
	 * @param capabilitiesEU aare the capabilites
	 * @return A list of metricref
	 */
	public List<String> getMetriRef(CapabilitiesEU capabilitiesEU) {
		List<String> refs = new ArrayList<String>();
		for (int i = 0; i < capabilitiesEU.getCapabilities().size(); i++) {
			List<SecurityControlEU> controls = capabilitiesEU.getCapabilities()
					.get(i).getFrameworks().get(0).getSecurityControls();
			for (int j = 0; j < controls.size(); j++) {
				for (int k = 0; k < controls.get(j).getMetrics().size(); k++) {
					refs.add(controls.get(j).getMetrics().get(k).getId());
				}
			}
		}
		return refs;
	}

	/**
	 * Provide a list of id
	 * @param capabilitiesEU are the capabilities
	 * @return a list of capability id
	 */

	public List<String> getCapabilityId(CapabilitiesEU capabilitiesEU) {
		List<String> caps = new ArrayList<String>();
		for (int i = 0; i < capabilitiesEU.getCapabilities().size(); i++) {
			caps.add(capabilitiesEU.getCapabilities().get(i).getId());
		}
		return caps;
	}


	/**
	 * Create a map, associates at metric name the metric object
	 * @param agreement is the Agreement object
	 * @param capability is the capability within the metrics
	 * @return a map of metric name/metric object
	 */

	public Map<String, MetricEU> getMetricBase(AgreementOffer agreement,
			CapabilityEU capability) {
		All all = agreement.getTerms().getAll();
		List<Term> terms = all.getAll();
		Map<String, MetricEU> metricsMap = new HashMap<String, MetricEU>();
		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof ServiceProperties) {
				ServiceProperties serv = (ServiceProperties) terms.get(i);
				String name = serv.getName().substring(
						serv.getName().indexOf("'") + 1,
						serv.getName().lastIndexOf("'"));

				if (name.equals(capability.getId())) {
					List<Variable> variables = serv.getVariableSet()
							.getVariables();
					MetricEU metric;

					for (int j = 0; j < variables.size(); j++) {
						Variable var = variables.get(j);

						if (metricsMap.containsKey(var.getMetric())) {
							metric = metricsMap.get(var.getMetric());

						} else {
							metric = new MetricEU(var.getMetric(), "", "", "",
									"", "", new ArrayList<String>());
						}
						String[] securities = var.getLocation().split("\\|");
						boolean found = false;
						for (int k = 0; k < securities.length; k++) {
							String sec = securities[k].substring(
									securities[k].indexOf("'") + 1,
									securities[k].lastIndexOf("'"));

							if (!metric.getSecurityControls().contains(sec)
									&& Utility
									.containSecurityControl(capability
											.getFrameworks().get(0)
											.getSecurityControls(), sec)) {
								List<String> sc = metric.getSecurityControls();
								sc.add(sec);
								metric.setSecurityControls(sc);
								found = true;
							}
						}
						if (found)
							metricsMap.put(var.getMetric(), metric);
					}
				}
			}
		}
		return metricsMap;
	}

	/**
	 * Create a map, associates at metric name the metric object withou security control ref
	 * @param agreement The Agreement Object
	 * @param capability within the metric
	 * @return a map of metric name/Metric Object
	 */

	public Map<String, MetricEU> getMetricBaseSla(AgreementOffer agreement,
			CapabilityEU capability) {
		All all = agreement.getTerms().getAll();
		List<Term> terms = all.getAll();
		Map<String, MetricEU> metricsMap = new HashMap<String, MetricEU>();
		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof ServiceProperties) {
				ServiceProperties serv = (ServiceProperties) terms.get(i);
				String name = serv.getName().substring(
						serv.getName().indexOf("'") + 1,
						serv.getName().lastIndexOf("'"));

				if (name.equals(capability.getId())) {
					List<Variable> variables = serv.getVariableSet()
							.getVariables();
					MetricEU metric;

					for (int j = 0; j < variables.size(); j++) {
						Variable var = variables.get(j);

						if (metricsMap.containsKey(var.getMetric())) {
							metric = metricsMap.get(var.getMetric());

						} else {
							metric = new MetricEU(var.getMetric(), "", "", "",
									"", "", new ArrayList<String>());
						}

						metricsMap.put(var.getMetric(), metric);
					}
				}
			}
		}
		return metricsMap;
	}

	/**
	 * Return the slow to show of the user starting from info selected (capabilites)
	 * @param offer the Agreement object
	 * @param capabilitiesEU The element selected from the user
	 * @return The slo info
	 */ 

	SLOStep buildSLOStep(AgreementOffer offer, CapabilitiesEU capabilitiesEU) {
		List<CapabilitySLOStep> capabilitiesSLOStep = new ArrayList<CapabilitySLOStep>();
		System.out.println("capabilitiesEU.getCapabilities().size() " + capabilitiesEU.getCapabilities().size());
		for (int i = 0; i < capabilitiesEU.getCapabilities().size(); i++) {

			CapabilityEU capability = capabilitiesEU.getCapabilities().get(i);
			Map<String, MetricEU> metrics = getMetricBase(offer, capability);
			addSLOInfo(offer, metrics);
			addMetricDefinition(offer, metrics);
			List<MetricEU> values = new ArrayList<MetricEU>(metrics.values());
			List<FrameworkSLO> frameworks = new ArrayList<FrameworkSLO>();
			frameworks.add(new FrameworkSLO(capability.getFrameworks().get(0)
					.getId(), values));
			capabilitiesSLOStep.add(new CapabilitySLOStep(capability.getId(),
					capability.getId(), frameworks));
		}
		return new SLOStep(capabilitiesEU.getIdsla(), capabilitiesSLOStep);

	}

	/**
	 * Provide a Map with assocations metric name / metric ref
	 * @param offer The Agreement object
	 * @return a map
	 */

	private Map<String, String> getMetricNameRef(AgreementOffer offer) {
		Map<String, String> metricNameRef = new HashMap<String, String>();

		All all = offer.getTerms().getAll();
		List<Term> terms = all.getAll();

		for (int i = 0; i < terms.size(); i++) {

			if (terms.get(i) instanceof ServiceProperties) {
				ServiceProperties properties = (ServiceProperties) terms.get(i);

				properties.getVariableSet().getVariables();
				int j = 0;
				while (j < properties.getVariableSet().getVariables().size()) {
					metricNameRef
					.put(properties.getVariableSet().getVariables()
							.get(j).getName(), properties
							.getVariableSet().getVariables().get(j)
							.getMetric());
					j++;
				}
			}
		}
		return metricNameRef;
	}

	/**
	 * Provide the full list of metric ref of the Agreement
	 * @param offer is the Agreement object
	 * @return a list of metric ref
	 */
	public List<String> getMetricRef(AgreementOffer offer) {
		List<String> metrics = new ArrayList<String>();
		Map<String, String> metricsMap = getMetricNameMapping(offer);
		metrics.addAll(metricsMap.keySet());
		return metrics;

	}

	/**
	 * Provide a map that asociates metric ref to metric name
	 * @param offer is the Agreement object
	 * @return a map of string
	 */

	public Map<String, String> getMetricNameMapping(AgreementOffer offer) {
		Map<String, String> metricNameMapping = new HashMap<String, String>();

		All all = offer.getTerms().getAll();
		List<Term> terms = all.getAll();

		for (int i = 0; i < terms.size(); i++) {

			if (terms.get(i) instanceof ServiceProperties) {
				ServiceProperties properties = (ServiceProperties) terms.get(i);

				properties.getVariableSet().getVariables();
				int j = 0;
				while (j < properties.getVariableSet().getVariables().size()) {
					metricNameMapping.put(properties.getVariableSet()
							.getVariables().get(j).getMetric(), properties
							.getVariableSet().getVariables().get(j).getName());
					j++;
				}
			}
		}

		return metricNameMapping;
	}

	/**
	 * Add the slo to the Agreement object
	 * @param offer the agreement object
	 * @param metrics is the map of the metric with the new slo value
	 */

	public void addSLOInfo(AgreementOffer offer, Map<String, MetricEU> metrics) {
		All all = offer.getTerms().getAll();
		List<Term> terms = all.getAll();
		Map<String, String> metricNameRef = getMetricNameRef(offer);

		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof GuaranteeTerm) {
				GuaranteeTerm g = (GuaranteeTerm) terms.get(i);
				ObjectiveList objectiveList = g.getServiceLevelObjective()
						.getCustomServiceLevel().getObjectiveList();
				List<SLOType> slos = objectiveList.getSLO();
				for (int j = 0; j < slos.size(); j++) {

					if (metrics.containsKey(metricNameRef.get(slos.get(j)
							.getMetricREF()))) {
						SLOType slo = slos.get(j);
						MetricEU metric = metrics.get(metricNameRef.get(slos
								.get(j).getMetricREF()));
						if (metric == null)
							metric = new MetricEU("", "", "", "", "", "",
									new ArrayList<String>());

						String operator = slo.getSLOexpression()
								.getOneOpExpression().getOperator().toString();
						String operand = slo.getSLOexpression()
								.getOneOpExpression().getOperand().toString();
						String importance = slo.getImportanceWeight().name();
						metric.setOperand(operand);
						metric.setOperator(Utility.getSLOOperand(operator));
						metric.setImportance(importance);
						metrics.put(
								metricNameRef.get(slos.get(j).getMetricREF()),
								metric);
					}
				}
			}
		}
	}

	/**
	 * Get the slo from Agreement
	 * @param offer the agreement object
	 * @return a list of slo info
	 */

	public List<SLOType> getSLOInfo(AgreementOffer offer) {
		All all = offer.getTerms().getAll();
		List<Term> terms = all.getAll();
		List<SLOType> allSlos = new ArrayList<SLOType>();
		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof GuaranteeTerm) {
				GuaranteeTerm g = (GuaranteeTerm) terms.get(i);
				ObjectiveList objectiveList = g.getServiceLevelObjective()
						.getCustomServiceLevel().getObjectiveList();
				List<SLOType> slos = objectiveList.getSLO();
				allSlos.addAll(slos);
			}
		}
		return allSlos;
	}

	/**
	 * Provide a framework for the capability
	 * @param capability the capability
	 * @return the framework of the capability
	 */

	public FrameworkEU getFramework(CapabilityType capability) {
		eu.specs.datamodel.sla.sdt.ControlFramework controlFramework = capability
				.getControlFramework();
		List<SecurityControlEU> securityControl = new ArrayList<SecurityControlEU>();
		return new FrameworkEU(controlFramework.getId(),
				controlFramework.getFrameworkName(), securityControl);
	}

	/**
	 * Provide the security control within the capability
	 * @param capability is the capability object
	 * @return the framework with security control
	 */

	public FrameworkEU getSecurities(CapabilityType capability) {
		ControlFramework controlFramework = capability.getControlFramework();
		List<SecurityControlEU> securityControl = new ArrayList<SecurityControlEU>();
		for (int i = 0; i < controlFramework.getSecurityControl().size(); i++) {
			AbstractSecurityControl abstractControl = controlFramework
					.getSecurityControl().get(i);
			if (abstractControl instanceof NISTSecurityControl) {
				NISTSecurityControl nistControl = (NISTSecurityControl) abstractControl;
				SecurityControlEU current = new SecurityControlEU(
						nistControl.getId(), nistControl.getName(),
						nistControl.getControlDescription(), nistControl
						.getImportanceWeight().value());
				securityControl.add(current);
			} else {
				CCMSecurityControl ccmControl = (CCMSecurityControl) abstractControl;
				SecurityControlEU current = new SecurityControlEU(
						ccmControl.getId(), ccmControl.getName(),
						ccmControl.getControlDescription(), ccmControl
						.getImportanceWeight().value());
				securityControl.add(current);
			}

		}
		return new FrameworkEU(controlFramework.getId(),
				controlFramework.getFrameworkName(), securityControl);
	}

	/**
	 * Provide the Agreement object for the service
	 * @param name is the service's name
	 * @return an Agreement object
	 */
	private AgreementOffer getTemplateObject(String name, HttpServletRequest request) {
		AgreementOffer offer = null;
		System.out.println("Session: "+request.getSession().getAttribute("profile_username"));
		Object username = request.getSession().getAttribute("profile_username");
		if (servicesUrl.containsKey(name)) {

			RestTemplate rest = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			HttpEntity entity = new HttpEntity(null, headers);

			String xml = "";
			if(username == null){
				ResponseEntity<String> response = rest.exchange(
						servicesUrl.get(name), HttpMethod.POST, entity,
						String.class);
				xml = response.getBody();
			}else{
				ResponseEntity<String> response = rest.exchange(
						servicesUrl.get(name)+"?username="+username.toString(), HttpMethod.POST, entity,
						String.class);
				xml = response.getBody();
			}
			servicesXml.put(name, xml);
			offer = buildOfferFromXml(xml);
		}
		return offer;
	}

	/**
	 * Provide the Agreement object for the sla with id
	 * @param id is the slas'id
	 * @return an Agreement object
	 */

	public AgreementOffer getTemplateObjectFromId(String id) {
		AgreementOffer offer = null;
		RestTemplate rest = new RestTemplate();

		String xml = (String) rest.getForObject(cloudSlaEndpoint + id,
				String.class);

		offer = buildOfferFromXml(xml);

		return offer;
	}

	/**
	 * Provide the xml for the service
	 * @param name is the services's name
	 * @return an xml
	 */

	public String getTemplateXML(String name) {
		String xml = null;

		if (servicesUrl.containsKey(name)) {

			RestTemplate rest = new RestTemplate();
			xml = (String) rest.getForObject(servicesUrl.get(name),
					String.class);
		}
		return xml;
	}

	/**
	 * Build an Agreement object
	 * @param xml is the sla
	 * @return an Agreement object
	 */

	public AgreementOffer buildOfferFromXml(String xml) {
		AgreementOffer offer = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(AgreementOffer.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			offer = (AgreementOffer) unmarshaller.unmarshal(new StringReader(
					xml));
		} catch (JAXBException e) {
			System.out.println("JAXBException "+e.getMessage());

			logger.info(e);
		}
		return offer;
	}

	public String buildXmlFromOffer(AgreementOffer offer) throws JAXBException {
			JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(offer, sw);
			return sw.toString();
	}



	/**
	 * Add to the metic object the definiton
	 * @param agreement the Agreement object
	 * @param metrics are the object to add definitions
	 */

	private void addMetricDefinition(AgreementOffer agreement,
			Map<String, MetricEU> metrics) {
		All all = agreement.getTerms().getAll();
		List<Term> terms = all.getAll();
		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof ServiceDescriptionTerm) {
				ServiceDescriptionTerm serviceDescTerm = (ServiceDescriptionTerm) terms
						.get(i);
				ServiceDescriptionType servDesc = serviceDescTerm
						.getServiceDescription();
				List<AbstractMetricType> metricType = servDesc
						.getSecurityMetrics().getSecurityMetric();
				for (int j = 0; j < metricType.size(); j++) {
					if (metrics.containsKey(metricType.get(j).getReferenceId())) {
						MetricEU metric = metrics.get(metricType.get(j)
								.getReferenceId());
						if (metric == null)
							metric = new MetricEU("", "", "", "", "", "",
									new ArrayList<String>());
						metric.setDescription(metricType.get(j)
								.getAbstractMetricDefinition().getDefinition());
						metric.setName(metricType.get(j).getName());
						metrics.put(metricType.get(j).getReferenceId(), metric);
					}
				}
			}
		}
	}

	/**
	 * Provide all the capabilities of the Agreement
	 * @param agreement the Agreement object
	 * @return the capabilities
	 */

	public CapabilitiesEU getAllCapabilities(AgreementOffer agreement) {
		List<CapabilityEU> listCapabilityEU = new ArrayList<CapabilityEU>();
		All all = agreement.getTerms().getAll();
		List<Term> terms = all.getAll();
		CapabilitiesEU capabilitiesEU = null;
		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof ServiceDescriptionTerm) {
				ServiceDescriptionTerm serviceDescTerm = (ServiceDescriptionTerm) terms
						.get(i);
				ServiceDescriptionType servDesc = serviceDescTerm
						.getServiceDescription();

				Capabilities capabilities = servDesc.getCapabilities();

				if (capabilities != null) {
					List<CapabilityType> capabilityList = capabilities
							.getCapability();

					for (int j = 0; j < capabilityList.size(); j++) {
						FrameworkEU framework = getFramework(capabilityList
								.get(j));
						List<FrameworkEU> frameworks = new ArrayList<FrameworkEU>();
						frameworks.add(framework);
						CapabilityEU capability = new CapabilityEU(
								capabilityList.get(j).getId(), capabilityList
								.get(j).getName(), capabilityList
								.get(j).getDescription(), frameworks);
						listCapabilityEU.add(capability);
					}
					capabilitiesEU = new CapabilitiesEU(agreement.getName(),
							listCapabilityEU);
				}
			}
		}
		return capabilitiesEU;
	}

	/**
	 * Provide the capability with an id
	 * @param agreement the Agreement object
	 * @param ids list of id
	 * @return Capabilities
	 */

	private CapabilitiesEU getCapabilities(AgreementOffer agreement,
			List<IdList> ids) {
		List<CapabilityEU> listCapabilityEU = new ArrayList<CapabilityEU>();
		All all = agreement.getTerms().getAll();
		List<Term> terms = all.getAll();
		CapabilitiesEU capabilitiesEU = null;
		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof ServiceDescriptionTerm) {
				ServiceDescriptionTerm serviceDescTerm = (ServiceDescriptionTerm) terms
						.get(i);
				ServiceDescriptionType servDesc = serviceDescTerm
						.getServiceDescription();
				Capabilities capabilities = servDesc.getCapabilities();
				List<CapabilityType> capabilityList = capabilities
						.getCapability();

				for (int j = 0; j < capabilityList.size(); j++) {
					FrameworkEU framework = getSecurities(capabilityList.get(j));
					for (int k = 0; k < ids.size(); k++) {
						if (capabilityList.get(j).getId()
								.equals(ids.get(k).getId())) {
							List<FrameworkEU> frameworks = new ArrayList<FrameworkEU>();
							frameworks.add(framework);
							CapabilityEU capability = new CapabilityEU(
									capabilityList.get(j).getId(),
									capabilityList.get(j).getName(),
									capabilityList.get(j).getDescription(),
									frameworks);
							listCapabilityEU.add(capability);
						}
					}
				}
				capabilitiesEU = new CapabilitiesEU(agreement.getName(),
						listCapabilityEU);
			}
		}
		return capabilitiesEU;
	}

	/**
	 * Provide the capabilities object from the capabilities id and sla id
	 * @param capabilities within capabilities id and sla id
	 * @return Capability object
	 */

	public CapabilitiesEU getCapabilityEUFromChoosed(
			ChoosedCapabilities capabilities) {
		AgreementOffer offer = getTemplateObjectFromId(capabilities.getIdsla());

		return getCapabilities(offer, capabilities.getCapabilities());

	}

	/**
	 * Return the slo from the capabilities
	 * @param capabilitiesEU capabilities
	 * @return SLO info
	 */

	public SLOStep getSloStepFromCapabilities(CapabilitiesEU capabilitiesEU) {
		AgreementOffer offer = getTemplateObjectFromId(capabilitiesEU
				.getIdsla());

		return buildSLOStep(offer, capabilitiesEU);
	}

	/**
	 * Provide metrics detail plus info from Agreement
	 * @param sla tha Agreement object
	 * @param metricsMap the map of metric details
	 */

	public void addInfoForMonitoring(AgreementOffer sla,
			Map<String, Metricsdetail> metricsMap) {
		CapabilitiesEU capabilitiesEU = getAllCapabilities(sla);
		Map<String, String> mapMetricName = getMetricNameMapping(sla);
		if (capabilitiesEU != null)
			for (int i = 0; i < capabilitiesEU.getCapabilities().size(); i++) {
				CapabilityEU capability = capabilitiesEU.getCapabilities().get(
						i);
				Map<String, MetricEU> metrics = getMetricBaseSla(sla,
						capability);
				addSLOInfo(sla, metrics);
				addMetricDefinition(sla, metrics);
				Iterator it = metrics.entrySet().iterator();

				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();

					MetricEU metric = (MetricEU) pair.getValue();
					String metricRef = mapMetricName.get(metric.getId());

					if (metricsMap.containsKey(metricRef)) {

						Metricsdetail details = metricsMap.get(metricRef);
						details.setCapabilityname(capability.getName());
						details.setMetricname(metric.getName());
						metricsMap.put(metricRef, details);

					} else {
						String sloOperator = metric.getOperator();
						String sloOperand = metric.getOperand();
						Metricsdetail details = new Metricsdetail(
								capability.getName(), metric.getName(), sloOperator, sloOperand,
								"----------------", "Not Yet Available", "");
						metricsMap.put(metricRef, details);
					}
				}
			}
	}
}
