package specs.sws_application.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesManager {

	public static String getProperty(String key){
		Properties prop = new Properties();
		try {

			//load a properties file from class path, inside static method
			try{
				prop.load(new FileInputStream("/opt/apache-tomcat-7/webapps/specs_app.properties"));
			}catch(FileNotFoundException e){
				InputStream input = null;
				input = PropertiesManager.class.getClassLoader().getResourceAsStream("specs_app.properties");
				if(input==null){
					System.out.println("Sorry, unable to find specs_app.properties");
					return "";
				}
				//load a properties file from class path, inside static method
				prop.load(input);
			}
			String propertyValue = prop.getProperty(key);
			if(propertyValue.contains("$")){
				String[] innerValue = propertyValue.split("\\$\\{")[1].split("\\}");
				return getProperty(innerValue[0])+innerValue[1];
			}else{
				return propertyValue;
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return "";
	}
}
