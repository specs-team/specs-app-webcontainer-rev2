package specs.sws_application.utility;

import java.util.List;

import eu.specs.datamodel.sla.sdt.OneOpOperator;
import specs.sw_application.frontend.entities.SecurityControlEU;

public class Utility {
	
	private Utility() {
		
	}
	
	public static String lastElement(String url, String separator) {

		if (url.contains(separator) && !url.endsWith(separator)) {
			String [] tokens = url.split(separator);
			if (tokens.length > 0)
				return tokens[tokens.length - 1];
		}
		return "";
	}
	
	public static String secondLastElement(String url, String separator) {

		return url.substring(0, url.lastIndexOf(separator));
	}
	
	public static boolean containSecurityControl(List<SecurityControlEU> securities, String id) {
		for (int i = 0; i < securities.size(); i++) {
			if (securities.get(i).getId().equals(id))
				return true;
		}
		return false;
	}
	
	public static String getSLOOperand(String op) {
			 if ("EQUAL".equals(op))
				 return "eq";
			 else if ("NOT_EQUAL".equals(op))
				 return "neq";
			 else if ("LESS_THAN".equals(op))
				 return "lt";
			 else if ("GREATER_THAN".equals(op))
				 return "gt";
			 else if ("LESS_THAN_OR_EQUAL".equals(op))
				 return "le";
			 else if ("GREATER_THAN_OR_EQUAL".equals(op))
				 return "ge";
			 else
				 return "";
	}
	
	public static OneOpOperator convertTOSLOOperator(String op) {
		 if ("eq".equals(op))
			 return OneOpOperator.EQUAL;
		 else if ("neq".equals(op))
			 return OneOpOperator.NOT_EQUAL;	 
		 else if ("lt".equals(op))
			 return OneOpOperator.LESS_THAN;
		 else if ("gt".equals(op))
			 return OneOpOperator.GREATER_THAN;
		 else if ("le".equals(op))
			 return OneOpOperator.LESS_THAN_OR_EQUAL;
		 else if ("ge".equals(op))
			 return OneOpOperator.GREATER_THAN_OR_EQUAL;
		 return OneOpOperator.EQUAL;
}
	
	public static String getFromQuotes(String op) {
		String result = op.substring(
				op.indexOf("'") + 1,
				op.lastIndexOf("'"));
		return result;
	}
}
