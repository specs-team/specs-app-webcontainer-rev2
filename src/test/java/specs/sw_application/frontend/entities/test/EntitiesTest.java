package specs.sw_application.frontend.entities.test;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import specs.sw_application.frontend.entities.CapabilitiesEU;
import specs.sw_application.frontend.entities.CapabilityEU;
import specs.sw_application.frontend.entities.CapabilitySLOStep;
import specs.sw_application.frontend.entities.ChoosedCapabilities;
import specs.sw_application.frontend.entities.CollectionType;
import specs.sw_application.frontend.entities.FrameworkEU;
import specs.sw_application.frontend.entities.FrameworkSLO;
import specs.sw_application.frontend.entities.IdList;
import specs.sw_application.frontend.entities.Implement;
import specs.sw_application.frontend.entities.MetricEU;
import specs.sw_application.frontend.entities.MetricSLO;
import specs.sw_application.frontend.entities.MonitorDetails;
import specs.sw_application.frontend.entities.MonitorDetails.Metricsdetail;
import specs.sw_application.frontend.entities.NodesInfo;
import specs.sw_application.frontend.entities.NodesInfo.Node;
import specs.sw_application.frontend.entities.OfferChoosed;
import specs.sw_application.frontend.entities.OfferInfo;
import specs.sw_application.frontend.entities.OffersInfo;
import specs.sw_application.frontend.entities.PlanImplementations;
import specs.sw_application.frontend.entities.SLOStep;
import specs.sw_application.frontend.entities.SecurityControlEU;
import specs.sw_application.frontend.entities.ServiceEU;
import specs.sw_application.frontend.entities.ServicesEU;
import specs.sw_application.frontend.entities.SlaId;
import specs.sw_application.frontend.entities.SlaIds;
import specs.sw_application.frontend.entities.Synthetic;
import specs.sw_application.frontend.entities.Synthetic.Synth;

public class EntitiesTest {

	@Test
	public void TestGetOfferInfo() {
		OfferInfo info = new OfferInfo("1", "infotest", "http://localhost:8000");
		assertEquals("ID ", "1", info.getId());
		assertEquals("Name ", "infotest", info.getName());
		assertEquals("URL", "http://localhost:8000", info.getUrl());
	}
	
	@Test
	public void TestSetOfferInfo() {
		OfferInfo info = new OfferInfo("1", "infotest", "http://localhost:8000");
		info.setId("2");
		info.setName("infotest2");
		info.setUrl("http://localhost:9000");
		
		assertEquals("ID ", "2", info.getId());
		assertEquals("Name ", "infotest2", info.getName());
		assertEquals("URL", "http://localhost:9000", info.getUrl());
	}
	
	@Test
	public void TestCapabilitiesEU() {
		CapabilitiesEU caps = new CapabilitiesEU();
		caps.setIdsla("56FD6215E4B09AECF5325352");
		CapabilityEU cap = new  CapabilityEU();
		cap.setDescription("Capability of surviving to security incidents involving a web server, by implementing proper strategies aimed at preserving business continuity, achieved through redundancy and/or diversity.");
		cap.setId("webpool");
		cap.setName("Web Resilience");
		cap.setFrameworks(new ArrayList<FrameworkEU>());
		List<CapabilityEU> listCap = new ArrayList<CapabilityEU>();
		listCap.add(cap);
		caps.setCapabilities(listCap);
		
		listCap = caps.getCapabilities();
		
		assertEquals("IDSLA ", "56FD6215E4B09AECF5325352", caps.getIdsla());
		assertEquals("Size list", 1, caps.getCapabilities().size());
		
		cap = listCap.get(0);
		
		assertEquals("Capability name", "Web Resilience", cap.getName());
		assertEquals("Capability id", "webpool", cap.getId());
		assertEquals("Capability Desc", "Capability of surviving to security incidents involving a web server, by implementing proper strategies aimed at preserving business continuity, achieved through redundancy and/or diversity.", cap.getDescription());
		assertEquals("Framework number", 0, cap.getFrameworks().size());
		
		caps = new CapabilitiesEU("56FD6215E4B09AECF5325353", listCap);
		assertEquals("IDSLA ", "56FD6215E4B09AECF5325353", caps.getIdsla());
		
		cap = new  CapabilityEU("webpool", "Web Resilience", "Capability of surviving to security incidents involving a web server, by implementing proper strategies aimed at preserving business continuity, achieved through redundancy and/or diversity.", new ArrayList<FrameworkEU>());
		assertEquals("Capability name", "Web Resilience", cap.getName());
		assertEquals("Capability id", "webpool", cap.getId());
		assertEquals("Capability Desc", "Capability of surviving to security incidents involving a web server, by implementing proper strategies aimed at preserving business continuity, achieved through redundancy and/or diversity.", cap.getDescription());
		assertEquals("Framework number", 0, cap.getFrameworks().size());
	}
	
	@Test
	public void TestMetricEU() {
		List<String> securities = new ArrayList<String>();
		securities.add("WEBPOOL_NIST_CP_2");
		securities.add("WEBPOOL_NIST_SC_5");

		MetricEU metric = new MetricEU("level_of_redundancy_m1", "specs_webpool_M1",
				"This abstract metric expresses the number of replicas of a software component that are set-up and kept active during system operation",
				"MEDIUM", "geq", "1", securities);
		
		assertEquals("Metric ID", "level_of_redundancy_m1", metric.getId());
		assertEquals("Metric Name", "specs_webpool_M1", metric.getName());
		assertEquals("Metric Desc", "This abstract metric expresses the number of replicas of a software component that are set-up and kept active during system operation", metric.getDescription());
		assertEquals("Metric importance", "MEDIUM", metric.getImportance());
		assertEquals("Metric Operator", "geq", metric.getOperator());
		assertEquals("Metric Operand", "1", metric.getOperand() );
		assertEquals("Metric n securities", 2, metric.getSecurityControls().size());
		
		securities = metric.getSecurityControls();
		
		assertEquals("Security ", true, securities.contains("WEBPOOL_NIST_CP_2"));
		assertEquals("Security ", true, securities.contains("WEBPOOL_NIST_SC_5"));
		
		metric.setId("level_of_diversity_m2");
		metric.setName("specs_webpool_M2");
		metric.setDescription("This abstract metric expresses the number of replicas of a software component that are set-up and kept active during system operation");
		metric.setImportance("LOW");
		metric.setOperand("2");
		metric.setOperator("lt");
		
		securities.clear();
		securities.add("WEBPOOL_NIST_SC_29");
		metric.setSecurityControls(securities);
		
		assertEquals("Metric ID", "level_of_diversity_m2", metric.getId());
		assertEquals("Metric Name", "specs_webpool_M2", metric.getName());
		assertEquals("Metric Desc", "This abstract metric expresses the number of replicas of a software component that are set-up and kept active during system operation", metric.getDescription());
		assertEquals("Metric importance", "LOW", metric.getImportance());
		assertEquals("Metric Operator", "lt", metric.getOperator());
		assertEquals("Metric Operand", "2", metric.getOperand() );
		assertEquals("Metric n securities", 1, metric.getSecurityControls().size());
		securities = metric.getSecurityControls();
		
		assertEquals("Security ", true, securities.contains("WEBPOOL_NIST_SC_29"));
	}
	
	@Test
	public void TestMonitorDetails() {
		
	}
	
	@Test
	public void TestSynthetic() {
		Synth synt = new Synth("specs_webpool_M2", "gt", "1", "LOW");
		List<Synth> list = new ArrayList<Synth>();
		list.add(synt);
		Synthetic synthetic = new Synthetic(list, "testId");
		List<Synth> listDum = synthetic.synth;
		assertEquals("Synt name", "specs_webpool_M2", listDum.get(0).name);
		assertEquals("Synt name", "gt", listDum.get(0).operation);
		assertEquals("Synt name", "1", listDum.get(0).value);
		assertEquals("Synt name", "LOW", listDum.get(0).importance);	
	}
	
	@Test
	public void TestSlaId() {
		SlaId slaId = new SlaId();
		slaId.setSla_id("56FD6215E4B09AECF5325352");
		assertEquals("SLAID ", "56FD6215E4B09AECF5325352", slaId.getSla_id());
		slaId = new SlaId("56FD6215E4B09AECF5325353");
		assertEquals("SLAID ", "56FD6215E4B09AECF5325353", slaId.getSla_id());
	}
	
	@Test
	public void TestSlaIds() {
		SlaIds ids = new SlaIds();
		List<IdList> slas = new ArrayList<IdList>();
		IdList id = new IdList();
		id.setId("56FD6215E4B09AECF5325353");
		assertEquals("IDLIST ", id.getId(), "56FD6215E4B09AECF5325353");
		id = new IdList("56FD6215E4B09AECF5325352");
		slas.add(id);
		ids.setSlas(slas);
		List<IdList> slasDum = ids.getSlas();
		IdList dum = slasDum.get(0);
		assertEquals("IDLIST ", dum.getId(), "56FD6215E4B09AECF5325352");
		ids = new SlaIds(slas);
		assertEquals("IDLIST ", ids.getSlas().size(), 1);
	}
	
	@Test
	public void TestServices() {
		ServicesEU services = new ServicesEU();
		assertEquals("SERVICES Empty", services.getServices().size(), 0);
		ServiceEU service = new ServiceEU();
		assertEquals("SERVICE Empty", service.getId(), "");
		ServiceEU serviceFull = new ServiceEU("ID1");
		assertEquals("SERVICE Full 1", serviceFull.getId(), "ID1");
		serviceFull.setId("ID2");
		assertEquals("SERVICE Full 2", serviceFull.getId(), "ID2");
		
		List<ServiceEU> listServ = new ArrayList<ServiceEU>();
		listServ.add(serviceFull);
		services = new ServicesEU(listServ);
		assertEquals("SERVICES Empty", services.getServices().size(), 1);
		listServ.add(service);
		services.setServices(listServ);
		assertEquals("SERVICES Empty", services.getServices().size(), 2);
	}
	
	@Test
	public void TestSecurityControEU() {
		SecurityControlEU secEmpty = new SecurityControlEU();
		secEmpty.setId("AIS-03");
		secEmpty.setDescription("Application and Interface Security - Data Integrity");
		secEmpty.setMetrics(new ArrayList<MetricSLO>());
		secEmpty.setName("Data input and output integrity");
		secEmpty.setWeight("LOW");
		assertEquals("Secuirty", "AIS-03", secEmpty.getId());
		assertEquals("Secuirty", "Data input and output integrity", secEmpty.getName());
		assertEquals("Secuirty", "Application and Interface Security - Data Integrity", secEmpty.getDescription());
		assertEquals("Secuirty", "LOW", secEmpty.getWeight());
		
		SecurityControlEU sec = new SecurityControlEU("AIS-03", "Application and Interface Security - Data Integrity", "Data input and output integrity", "LOW");
		MetricSLO sloEmpy = new MetricSLO();
		MetricSLO slo = new MetricSLO("level_of_diversity_m2", "LOW", "+", "1");
		List<MetricSLO> slos = new ArrayList<MetricSLO>();
		slos.add(slo);
		sec.setMetrics(slos);
		sec.setWeight("HIGH");
		assertEquals("METRIC SLO", "level_of_diversity_m2", sec.getMetrics().get(0).getId());
		assertEquals("METRIC SLO", "LOW", sec.getMetrics().get(0).getImportance());
		assertEquals("METRIC SLO", "+", sec.getMetrics().get(0).getOperator());
		assertEquals("METRIC SLO", "1", sec.getMetrics().get(0).getOperand());
		assertEquals("Secuirty", "AIS-03", sec.getId());
		assertEquals("Secuirty", "Application and Interface Security - Data Integrity", sec.getName());
		assertEquals("Secuirty", "Data input and output integrity", sec.getDescription());
		assertEquals("Secuirty", "HIGH", sec.getWeight());
		
		assertEquals("Secuirty", sec.getWeights().size(), 3);
	}
	
	@Test
	public void testPlanImplementations() {
		Implement impl = new Implement("5", "56FD6215E4B09AECF5325352", "ACTIVE", "eb0bd36b-74f0-429d-8e0c-b34f9c1895f7");
		List<Implement> impls = new ArrayList<Implement>();
		
		assertEquals("IMPLEMENT ", impl.getId(), "5");
		assertEquals("IMPLEMENT ", impl.getSlaid(), "56FD6215E4B09AECF5325352");
		assertEquals("IMPLEMENT ", impl.getState(), "ACTIVE");
		assertEquals("IMPLEMENT ", impl.getIdimpl(), "eb0bd36b-74f0-429d-8e0c-b34f9c1895f7");
		impl.setId("6");
		impl.setIdimpl("eb0bd36b-74f0-429d-8e0c-b34f9c1895f8");
		impl.setSlaid("56FD6215E4B09AECF5325353");
		impl.setState("READY");
		impls.add(impl);
		PlanImplementations plan = new PlanImplementations(impls);
		assertEquals("PLAN ", plan.getImplementations().size(), 1);
		Implement dum = plan.getImplementations().get(0);
		assertEquals("IMPLEMENT ", dum.getId(), "6");
		assertEquals("IMPLEMENT ", dum.getSlaid(), "56FD6215E4B09AECF5325353");
		assertEquals("IMPLEMENT ", dum.getState(), "READY");
		assertEquals("IMPLEMENT ", dum.getIdimpl(), "eb0bd36b-74f0-429d-8e0c-b34f9c1895f8");
	}
	
	@Test
	public void testOffersInfo() {
		List<OfferInfo> infoList = new ArrayList<OfferInfo>();
		OfferInfo info = new OfferInfo("1", "infotest", "http://localhost:8000");
		infoList.add(info);
		OffersInfo infos = new OffersInfo("56FD6215E4B09AECF5325353", infoList);
		assertEquals("OFFERS ", infos.getIdsla(), "56FD6215E4B09AECF5325353");
		assertEquals("OFFERS ", infos.getOffers(), infoList);
		infos.getOffers().clear();
		infos.setOffers(infoList);
		assertEquals("OFFERS ", infos.getOffers(), infoList);
	}
	
	@Test
	public void testOfferChoosed() {
		OfferChoosed offerChoosed = new OfferChoosed();
		assertEquals("OFFER CHOOSED", offerChoosed.getId(), null);
		assertEquals("OFFER CHOOSED", offerChoosed.getXml(), null);
		assertEquals("OFFER CHOOSED", offerChoosed.getIdsla(), null);
		offerChoosed = new OfferChoosed("56FD6215E4B09AECF5325353_offer1", "56FD6215E4B09AECF5325353", "<xml></xml>");
		assertEquals("OFFER CHOOSED", offerChoosed.getId(), "56FD6215E4B09AECF5325353_offer1");
		assertEquals("OFFER CHOOSED", offerChoosed.getXml(), "<xml></xml>");
		assertEquals("OFFER CHOOSED", offerChoosed.getIdsla(), "56FD6215E4B09AECF5325353");
		offerChoosed.setId("56FD6215E4B09AECF5325353_offer2");
		offerChoosed.setXml("<xml>1</xml>");
		offerChoosed.setIdsla("56FD6215E4B09AECF5325354");
		assertEquals("OFFER CHOOSED", offerChoosed.getId(), "56FD6215E4B09AECF5325353_offer2");
		assertEquals("OFFER CHOOSED", offerChoosed.getXml(), "<xml>1</xml>");
		assertEquals("OFFER CHOOSED", offerChoosed.getIdsla(), "56FD6215E4B09AECF5325354");
	}
	
	@Test
	public void testNodeInfo() {
		List<String> sec = new ArrayList<String>();
		List<String> ipr = new ArrayList<String>();
		String ipp = "244.244.244.244";
		sec.add("AIS-03");
		ipr.add("192.168.1.1");
		Node node = new Node("alpha", ipp, ipr, sec);
		List<Node> listNode = new ArrayList<Node>();
		listNode.add(node);
		NodesInfo info = new NodesInfo(listNode);
		assertEquals("NODE INFO", node.name, "alpha");
		assertEquals("NODE INFO", node.publicip, "244.244.244.244");
		assertEquals("NODE INFO", node.privateip.get(0), "192.168.1.1");
		assertEquals("NODE INFO", node.securities.get(0), "AIS-03");
		assertEquals("NODE INFO", info.nodes, listNode);
	}
	
	@Test
	public void testMonitorDetails() {
		Metricsdetail detail = new Metricsdetail();
		detail.setCapabilityname("Database and Backup as-a-service");
		detail.setMeasuredvalue("false");
		detail.setTimestamp("Not Yet Available");
		detail.setSloOperator("eq");
		detail.setSloOperand("true");
		detail.setMetricname("Integrity");
		
		assertEquals("MONITOR", detail.getCapabilityname(), "Database and Backup as-a-service");
		assertEquals("MONITOR", detail.getMeasuredvalue(), "false");
		assertEquals("MONITOR", detail.getTimestamp(), "Not Yet Available");
		assertEquals("MONITOR", detail.getSloOperator(), "eq");
		assertEquals("MONITOR", detail.getSloOperand(), "true");
		assertEquals("MONITOR", detail.getMetricname(), "Integrity");
		
		Metricsdetail detail1 = new Metricsdetail("End-to-end Encryption", "Confidentiality",
				"eq", "true", "-----", "1970-01-17 22:40:47.317", "");
		
		List<Metricsdetail> metrics = new ArrayList<Metricsdetail>();
		metrics.add(detail1);
		
		MonitorDetails monitor = new MonitorDetails();
		monitor.setMetricsdetails(metrics);
		
		assertEquals("MONITOR", monitor.getMetricsdetails().size(), 1);
		monitor = new MonitorDetails(metrics);
		assertEquals("MONITOR", monitor.getMetricsdetails().size(), 1);
		
	}
	
	@Test
	public void testFrameworkEU() {
		FrameworkEU frame = new FrameworkEU();
		List<SecurityControlEU> securityControls = new ArrayList<SecurityControlEU>();
		
		frame = new FrameworkEU("1","WEBPOOL_NIST_SC_29", securityControls);
		assertEquals("FRAMEWORKEU", frame.getId(), "1");
		assertEquals("FRAMEWORKEU", frame.getName(), "WEBPOOL_NIST_SC_29");
		assertEquals("FRAMEWORKEU", frame.getSecurityControls().size(), 0);
	}
	
	@Test
	public void FrameSLO() {
		List<String> securities = new ArrayList<String>();
		securities.add("WEBPOOL_NIST_CP_2");
		securities.add("WEBPOOL_NIST_SC_5");
		MetricEU metric = new MetricEU("level_of_redundancy_m1", "specs_webpool_M1",
				"This abstract metric expresses the number of replicas of a software component that are set-up and kept active during system operation",
				"MEDIUM", "geq", "1", securities);
		List<MetricEU> metrics = new ArrayList<MetricEU>();
		metrics.add(metric);
		FrameworkSLO frame = new FrameworkSLO("WEBPOOL_NIST_SC_29", metrics);
		assertEquals("FRAMEWORKSLO", frame.getMetrics().size(), 1);
		assertEquals("FRAMEWORKSLO", frame.getId(), "WEBPOOL_NIST_SC_29");
		
		metrics.clear();
		frame.setId("WEBPOOL_NIST_SC_28");
		frame.setMetrics(metrics);
		assertEquals("FRAMEWORKSLO", frame.getMetrics().size(), 0);
		assertEquals("FRAMEWORKSLO", frame.getId(), "WEBPOOL_NIST_SC_28");
		
		List<FrameworkSLO> frameworks = new ArrayList<FrameworkSLO>();
		frameworks.add(frame);
		
		CapabilitySLOStep cap = new CapabilitySLOStep("level_of_diversity_m2", "spec_web_pool2", frameworks);
		assertEquals("CapabilitySLOStep", cap.getId(), "level_of_diversity_m2");
		assertEquals("CapabilitySLOStep", cap.getName(), "spec_web_pool2");
		assertEquals("CapabilitySLOStep", cap.getFrameworks().size(), 1);
		cap.setId("level_of_diversity_m1");
		cap.setName("spec_web_pool1");
		frameworks.clear();
		cap.setFrameworks(frameworks);
		assertEquals("CapabilitySLOStep", cap.getId(), "level_of_diversity_m1");
		assertEquals("CapabilitySLOStep", cap.getName(), "spec_web_pool1");
		assertEquals("CapabilitySLOStep", cap.getFrameworks().size(), 0);
	}
	
	@Test
	public void testSloStep() {
		List<FrameworkSLO> frameworks = new ArrayList<FrameworkSLO>();
		CapabilitySLOStep cap = new CapabilitySLOStep("level_of_diversity_m2", "spec_web_pool2", frameworks);
		List<CapabilitySLOStep> caps = new ArrayList<CapabilitySLOStep>();
		caps.add(cap);
		SLOStep slo = new SLOStep("56FD6215E4B09AECF5325353", caps);
		assertEquals("SLOStep", slo.getIdsla(), "56FD6215E4B09AECF5325353");
		assertEquals("SLOStep", slo.getCapabilities().size(), 1);
		slo.setIdsla("56FD6215E4B09AECF5325354");
		caps.clear();
		slo.setCapabilities(caps);
		assertEquals("SLOStep", slo.getIdsla(), "56FD6215E4B09AECF5325354");
		assertEquals("SLOStep", slo.getCapabilities().size(), 0);
	}
	
	@Test
	public void testChoosedCapabilities() {
		ChoosedCapabilities cap = new ChoosedCapabilities();
		List<IdList> slas = new ArrayList<IdList>();
		IdList id = new IdList();
		id.setId("level_of_diversity_m2");
		slas.add(id);
		
		cap.setCapabilities(slas);
		cap.setIdsla("56FD6215E4B09AECF5325352");
		assertEquals("ChoosedCapabilities", cap.idsla, "56FD6215E4B09AECF5325352");
		assertEquals("ChoosedCapabilities", cap.getCapabilities().size(), 1);
	}
	
	
}
