package specs.sws_application.frontend.rest.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;

import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.client.response.MockRestResponseCreators;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import specs.sw_application.frontend.entities.CapabilitiesEU;
import specs.sw_application.frontend.entities.ServicesEU;
import specs.sws_application.frontend.rest.MainRestController;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration("server.port:18090")
@ContextConfiguration(classes = {MainRestController.class})
@Profile("test")
public class FrontendRestTest extends AbstractJUnit4SpringContextTests{
	
    @InjectMocks
    MainRestController controller = new MainRestController();

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(18889);
	private  Logger logger;
		
	public FrontendRestTest() {
		logger = (Logger) LogManager.getLogger(FrontendRestTest.class);
	}
	
	@Before
    public void setUp(){
    }

	@Test
	public void testServices() {
		
        final MainRestController myController = new MainRestController();
        
        final MockRestServiceServer mockServer = MockRestServiceServer.createServer(myController.getRestTemplate());

        ReflectionTestUtils.setField(myController, "rest", myController.getRestTemplate());

        final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(myController).build();
        
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>  <collection resource=\"sla-templates\" total=\"1\" items=\"1\" members=\"1\"> <itemList>http://apps.specs-project.eu:8080/slo-manager-api/sla-negotiation/sla-templates/WebPool_SVA</itemList></collection>";
		mockServer.expect(MockRestRequestMatchers.requestTo("http://localhost:18888/slo-manager-api/sla-negotiation/sla-templates/"))
        .andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
        .andRespond(MockRestResponseCreators.withSuccess(xml, MediaType.TEXT_XML));
		
		String templateXml = readStream(this.getClass().getResourceAsStream("/WebPool_SVA.xml"));
		
		mockServer.expect(MockRestRequestMatchers.requestTo("http://localhost:18888/sla-negotiation/sla-templates/WebPool_SVA"))
        .andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
        .andRespond(MockRestResponseCreators.withSuccess(templateXml, MediaType.TEXT_PLAIN));

        MvcResult mvcResult;
		try {
			mvcResult = mockMvc.perform(get("/services")
			        .contentType(MediaType.APPLICATION_JSON))
			        .andExpect(status().isOk()).andReturn();

			ObjectMapper mapper = new ObjectMapper();
			ServicesEU obj = mapper.readValue(mvcResult.getResponse().getContentAsString(), ServicesEU.class);
			assertEquals("SERVICES", obj.getServices().get(0).getId(),"WebPool_SVA");
			
			mvcResult = mockMvc.perform(get("/capabilities/WebPool_SVA")
			        .contentType(MediaType.APPLICATION_JSON))
			        .andExpect(status().isOk()).andReturn();
			
			CapabilitiesEU  objCap = mapper.readValue(mvcResult.getResponse().getContentAsString(), CapabilitiesEU.class);
			assertEquals("CAPABILITIES", objCap.getCapabilities().size(), 2);
			assertEquals("CAPABILITIES", objCap.getCapabilities().get(0).getId(), "WEBPOOL");
			assertEquals("CAPABILITIES", objCap.getCapabilities().get(1).getId(), "SVA");
		
			String caps = "{\"capabilities\":[{\"id\":\"WEBPOOL\"}],\"idsla\":\"571FF42CE4B057C3E72A10E2\"}";
			mvcResult = mockMvc.perform(post("/securities")
					.content(caps)
			        .contentType(MediaType.APPLICATION_JSON))
			        .andExpect(status().isOk()).andReturn();
			
			
			CapabilitiesEU  objCapsecu = mapper.readValue(mvcResult.getResponse().getContentAsString(), CapabilitiesEU.class);
			assertEquals("Securities", objCapsecu.getCapabilities().get(0).getFrameworks(), 3);
		
		} catch (Exception e) {
			logger.debug(e);
		}
		wireMockRule.shutdown();
        //mockServer.verify();
	}
	
	public static String readStream(InputStream is) {
        StringBuilder sb = new StringBuilder();
        try {
            Reader r = new InputStreamReader(is, "UTF-8");
            int c;
            while ((c = r.read()) != -1) {
                sb.append((char) c);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }
}
